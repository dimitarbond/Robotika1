%% KINEMATIKA NA ROBOTSKA RAKA
%% Voved
% p = [x; y; z; w] (x,y,z) - koordinati w - normirachki faktor
% px = x/w; py = y/w; pz = z/w
% ako w = 0, se koristi kako vektor na pravec
% F = [nx ox ax; ny oy ay; nz oz az] - proizvolen koordinaten s-m
% n,o,a - edinichni vektori
% ako proizvolniot koordinaten s-m ne e smesten vo koordinatniot pochetok 
% => F = [nx ox ax px; ny oy ay py; nz oz az pz; 0 0 0 1]
% n*o = n*a = a*0 = 0
% |n|=|o|=|a|= 1
%% Transformacii i rotacii
% Trans = [1 0 0 dx; 0 1 0 dy; 0 0 1 dz; 0 0 0 1] - koord. s-m 
% so translacija dx,dy,dz edinici soodvetno
% R(x, Theta) = [ 1,          0,           0, 0
%                0, cos(Theta), -sin(Theta), 0
%                0, sin(Theta),  cos(Theta), 0
%                0,          0,           0, 1]
% R(y, Theta) = [  cos(Theta), 0, sin(Theta), 0
%                           0, 1,          0, 0
%                 -sin(Theta), 0, cos(Theta), 0
%                           0, 0,          0, 1]
% R(z, Theta) = [ cos(Theta), -sin(Theta), 0, 0
%                 sin(Theta),  cos(Theta), 0, 0
%                          0,           0, 1, 0
%                          0,           0, 0, 1]
%% Theta i L(edinechen vektor)
% Ako R = [r11 r12 r13 
%          r21 r22 r23
%          r31 r32 r33]
% Theta = arccos((r11+r22+r33 - 1)/2)
% L = 1/(2*sin(Theta))*[r32-r23; r13-r31; r21-r12] = [lx; ly; lz]
% lx^2+ly^2+lz^2 = 1 
% inv(T) = [nx ny nz -P*n; ox oy oz -P*o; ax ay az -P*a; 0 0 0 1]
% P*n = px*nx + py*ny + pz*nz
% odreduvanje na pozicijata i orientacijata na rakata od robotot 
% se narekuva _direktna kinematika_
%% Cilindrichni koordinati
clc
clear variables
syms alpha r L
Tcyl = transl(0,0,L) * trotz(alpha) * transl(r,0,0);
disp(Tcyl)
% odrotiranje 
Todrotirano = simplify(Tcyl * trotz(-alpha));
disp(Todrotirano)
%% Sferni koordinati
clc 
clear variables
syms r beta gama
Tsph = simplify(trotz(gama)*troty(beta)*transl(0,0,r));
disp(Tsph)
%% ZYZ - Euler
clc 
clear variables
syms Phi Theta Psi
RotacijaZYZ = simplify(trotz(Phi)*troty(Theta)*trotz(Psi));
disp(RotacijaZYZ)
% Phi = arctg(sin(Phi)/cos(Phi)) = arctg(ay/ax)
% Phi = arctg(-ay/-ax)
% Psi = arctg(sin(Psi)/cos(Psi)) = arctg((-nx*sin(Phi)+ny*cos(Phi))/(-ox*sin(Phi)+oy*cos(Phi)))
% Theta = arctg(sin(Theta)/cos(Theta)) = arctg((ax*cos(Phi)+ay*sin(Phi))/az)
%% RPY - Roll, Pitch, Yaw
clc 
clear variables
syms Phi Theta Ni
RotacijaRPY = trotz(Phi)*troty(Theta)*trotx(Ni);
disp(simplify(RotacijaRPY))
% Phi = arctg(Sin(Phi)/cos(Phi))
% Theta = arctg(-nz/(nz*cos(Phi)+ny*sin(Phi)))
% Ni = arctg((ax*sin(Phi)-ay*cos(Phi))/(-ox*sin(Phi)+oy*cos(Phi)))
%% ZADACHI TEMA 2
%% 2.1
clc 
clear variables
% p = 8i + 2j + 5k i w=2
P = [8*2; 2*2; 5*2; 2];
disp(P)
% p edinechen
Pmodul = sqrt(8^2 + 5^2 + 2^2);
Pedinechen = [8/Pmodul; 2/Pmodul; 5/Pmodul; 0];
disp(Pedinechen)
%% 2.2 
clc
clear variables
% soodvetna matrica F
F = transl(3,4,7)*trotx(pi/4);
disp(F)
%% 2.3
clc 
clear variables
% soodvetna matrica F
F = [0 1 0 0; 1 0 0 0; 0 0 -1 0; 0 0 0 1];
F = transl(4,7,9)*F;
disp(F)
%% 2.4
clc
clear variables
% da se najde Pnova
Pstara = [3; 5; 7; 1];
Pnova = transl(2,3,4)*Pstara;
disp(Pnova)
%% 2.5
clc 
clear variables
% pridvizhuvanje na koordinaten s-m
B = [0 1 0 2; 1 0 0 4; 0 0 -1 6; 0 0 0 1];
Bnova = transl(5,2,6)*B;
disp(Bnova)
%% 2.6
clc
clear variables
% da se najde Fstara, 5,2,6
Fnova = [0 1 0 2; 1 0 0 4; 0 0 -1 6; 0 0 0 1];
% Fnova = transl(5,2,6)*Fstara
% transl(5,2,6)^-1*Fnova = Fstara;
Fstara = transl(5,2,6)\Fnova; % namesto inv() koristam \
disp(Fstara)
%% 2.7
clc 
clear variables
% dadena Fnova, da se najde pochetna 
Fstara = transl(2,4,6)\[0 1 0 0; 1 0 0 4; 0 0 -1 2; 0 0 0 1];
disp(Fstara)
%% 2.8
clc 
clear variables
% F = [0 ? 0 -2; 1 0 0 0; ? 0 ? -4; 0 0 0 1]
% treba modulite na n,o,a da bidat 1
% i skalarnite proizvodi da bidat nula
% nx*ax + ny*ay + nz*az = 0; nx=ax=ay=0 ny=1
% => nz*az = 0
% nx^2 + ny^2 + nz^2 = 1
% => nz = 0;
% => ox = +/- 1
% => az = +/- 1
%% 2.9
clc 
clear variables
% vektorska r-ka n x o = a
syms iv jv kv nx ny nz ox oy oz ax ay az
Vr = [iv jv kv; nx ny nz; ox oy oz];
Deter = det(Vr);
disp(Deter)
% se izramnuva so ax*iv+ay*jv*az*kv
% se dobiva ax = 0; ox = 1; ny = sqrt(2)/2
%% 2.10
clc 
clear variables
Rx = trotx(pi/2);
trplot(Rx)
Rz = trotz(pi/2);
trplot(Rz)
%% 2.11
clc 
% clf
clear variables
Rx = trotx(pi/2);
Rz = trotz(pi/2);
trplot(Rx) 
hold on;
trplot(Rz)
%% 2.12
clc 
clear variables
% matrica na rotacija okolu y-oska
syms Theta Pn Po Pa Px Py Pz
R = troty(Theta);
P = R * [Pn; Po; Pa; 1];
disp(P)
%% 2.13
clc
clear variables
% matriva na rotacija okolu z-oska
syms Theta Pn Po Pa Px Py Pz
R = trotz(Theta);
P = R * [Pn; Po; Pa; 1];
disp(P)
%% 2.14
clc 
clear variables
syms Theta
Rx = trotx(Theta);
n_o = Rx(1,1)*Rx(1,2)+Rx(2,1)*Rx(2,2)+Rx(3,1)*Rx(3,2);
disp(n_o);
n_a = Rx(1,1)*Rx(1,3)+Rx(2,1)*Rx(2,3)+Rx(3,1)*Rx(3,3);
disp(n_a);
a_o = Rx(1,3)*Rx(1,2)+Rx(2,3)*Rx(2,2)+Rx(3,3)*Rx(3,2);
disp(a_o);
% istoto treba da se napravi i za Ry, Rz
modul_n = Rx(1,1)^2 + Rx(2,1)^2 + Rx(3,1)^2;
disp(modul_n)
modul_o =simplify(Rx(1,2)^2 + Rx(2,2)^2 + Rx(3,2)^2);
disp(modul_o)
modul_a =simplify(Rx(1,3)^2 + Rx(2,3)^2 + Rx(3,3)^2);
disp(modul_a)
% istoto da se povtori za Ry, Rz
%% 2.15
clc 
clear variables
syms Theta
Rx = trotx(Theta);
Ry = troty(Theta);
Rz = trotz(Theta);
disp(simplify(det(Rx)))
disp(simplify(det(Ry)))
disp(simplify(det(Rz)))
%% 2.16
clc
clear variables
syms Theta
Ryz = troty(Theta)*trotz(Theta);
Rzy = trotz(Theta)*troty(Theta);
disp(Ryz)
disp(Rzy)
%% 2.17
clc
clear variables
syms Theta
P = trotx(Theta)*[2;3;4;1]; % koordinati na tochkata Pnoa
Pvrednost = subs(P, Theta, pi/4);
disp(Pvrednost)
%% 2.18
clc 
clear variables
syms Theta
P = trotz(Theta)*[3;5;7;1];
Pvrednost = subs(P, Theta, pi/6);
disp(simplify(Pvrednost))
%% 2.19
clc
clear variables
syms Theta
P = trotx(Theta)*[3;5;7;1];
Pvrednost = subs(P, Theta, 2*pi/3); % greshka vo zbirkata mesto 2 treba 5
disp(Pvrednost)
vpa(Pvrednost, '2')
%% 2.20
clc 
clear variables
syms Theta1
R = 1/9 * [1 -8 -4; 4 4 -7; 8 -1 4];
Theta = acosd((R(1,1)+R(2,2)+R(3,3) - 1)/2); % line#31
disp(Theta)
L = 1/(2*sin(Theta1))*[R(3,2)-R(2,3); R(1,3)-R(3,1); R(2,1)-R(1,2)]; %line#32
L_bez_decimali = subs(L,Theta1, pi/2);
disp(L_bez_decimali)
%% 2.21
clc
clear variables
syms Theta1
R = [1/2 sqrt(6)/4 -sqrt(6)/4
    -sqrt(6)/4 3/4 1/4
    sqrt(6)/4 1/4 3/4];
Theta = acosd((R(1,1)+R(2,2)+R(3,3) - 1)/2);
disp(Theta)
L = 1/(2*sin(Theta1))*[R(3,2)-R(2,3); R(1,3)-R(3,1); R(2,1)-R(1,2)]; 
L_bez_decimali = simplify(subs(L,Theta1, pi/3));
disp(L_bez_decimali)
% greshka vo zbirka treba -/+1/sqrt(2)*[0; 1; 1]
%% 2.22
clc 
clear variables
% orientacija na koordinaten s-m F
F0 = [1 0 0 9; 0 0 1 3; 0 -1 0 15; 0 0 0 3];
disp(F0)
% => W = 3
F1 = [1 0 0 3; 0 0 1 1; 0 -1 0 5; 0 0 0 1];
trplot(F1, 'color', 'r')
% bidejki prvata kolona e 1,0,0 sleduva deka rotacijata e po x-oska
syms Theta 
Rx = trotx(Theta);
Theta = atand(F1(3,2)/F1(2,2));
disp(Theta)
P = [3; 1; 5];
disp(P)
%% 2.23
clc 
clear variables
% rotacija i translacija
T = transl(0,3,0)*trotx(-pi/6)*trotz(pi/4)*transl(6,9,0);
P = T * [3; 4; -5; 1];
disp(P)
% ima greshka vo zbirka mesto Transl(6,9,0) ima 0,6,9
%% 2.24
clc
clear variables
T = trotz(pi/2)*transl(5,3,6)*trotx(pi/2);
P = T * [5; 3; 4; 1];
disp(P)
figure(1)
trplot(trotz(pi/2))
figure(2)
trplot(trotz(pi/2)*transl(5,3,6), 'color', 'g')
figure(3)
trplot(trotz(pi/2)*transl(5,3,6)*trotx(pi/2), 'color', 'r')
%% 2.25
clc 
clear variables
T = transl(5,3,6)*trotx(pi/2)*trotz(pi/2);
P = T*[2;3;5;1];
disp(P)
trplot(T)
%% 2.26
clc 
clear variables
syms Theta Phi Alpha Beta Gama
T = simplify(trotx(Gama)*trotz(Alpha)*trotx(Theta)*trotz(Phi)*troty(Beta));
T_90 = subs(T, [Theta Phi Alpha Beta Gama], [pi/2 pi/2 pi/2 pi/2 pi/2]);
disp(T_90)
P = T_90*[2;1;0;1];
disp(P)
%% 2.27
clc
clear variables
T = transl(0,3,0)*trotx(-pi/6)*trotz(pi/4)*transl(6,9,0);
P = T * [3; 4; -5; 1];
disp(P)
%% 2.28
clc 
clear variables
T0 = eye(4);
T1 = trotz(pi);
T2 = transl(-1,-1,0)*T1;
T3 = T2 * trotz(pi/2);
T4 = T3 * transl(2,1,0);
tranimate(T0,T1,'axis', [-5 5 -5 5 -5 5])
tranimate(T1,T2,'axis', [-5 5 -5 5 -5 5])
tranimate(T2,T3,'axis', [-5 5 -5 5 -5 5])
tranimate(T3,T4,'axis', [-5 5 -5 5 -5 5])
disp(T4)
%% 2.29
clc 
clear variables
T01 = transl(1,1,0);
T12 = T01 * trotz(pi);
T23 = T12 * transl(-2,-2,0);
T34 = T23 * trotz(pi/2);
tranimate(T01,T12,'axis', [-5 5 -5 5 -5 5])
tranimate(T12,T23,'axis', [-5 5 -5 5 -5 5])
tranimate(T23,T34,'axis', [-5 5 -5 5 -5 5])
disp(T34)
%% 2.30
clc 
clear variables
T1 = [0.527 -0.574 0.628 2
      0.369 0.819 0.439 5
      -0.766 0 0.643 3
       0 0 0 1];
T1inv = inv(T1);
disp(T1inv)
T2 = [0.92 0 0.39 5; 0 1 0 6; -0.39 0 0.92 2; 0 0 0 1];
T2inv = inv(T2);
disp(T2inv) % greshka ima vo zadachata chlen (3,4)
%% 2.31
clc 
clear variables
F12 = [1 0 0; 0 1/2 -sqrt(3)/2; 0 sqrt(3)/2 1/2];
F13 = [0 0 -1; 0 1 0; 1 0 0];
% F13 = F12 * F23 // pomnozheno so F12 od levo
F23 = F12 \ F13 ;
disp(F23)
%% 2.32
clc
clear variables
syms Theta Phi
R = troty(Theta) * trotz(Phi);
disp(R);
%% 2.33
clc
clear variables
syms r beta gama
Tsph = simplify(trotz(gama)*troty(beta)*transl(0,0,r)); % line#51
disp(Tsph)
Todrotirano_za_beta = Tsph * troty(-beta);
disp(Todrotirano_za_beta)
Todrotirano_plus_gama = Todrotirano_za_beta * trotz(-gama);
disp(simplify(Todrotirano_plus_gama))
%% 2.34
clc 
clear variables
Tsph = eye(4);
Tsph(1,4) = 3.1375;
Tsph(2,4) = 2.195;
Tsph(3,4) = 3.214;
[gama, beta, r] = cart2sph(Tsph(1,4),Tsph(2,4),Tsph(3,4));
gama_stepeni = radtodeg(gama);
beta_stepeni = radtodeg(beta);
disp(gama_stepeni)
disp(beta_stepeni) % se dobivaat razlichni vrednosti spored f-jata vo odnos na raka
disp(r)
gama_stepeni_raka = rad2deg(atan2(Tsph(2,4),Tsph(1,4)));
disp(gama_stepeni_raka)
beta_stepeni_raka = rad2deg(atan2(Tsph(1,4)*cosd(35)+Tsph(2,4)*sind(35), Tsph(3,4)));
disp(beta_stepeni_raka)
%% 2.35
clc
clear variables
% isto kako prethodnata
[gama, beta, r] = cart2sph(1.5, 1.5, sqrt(6)/2);
gamaS = radtodeg(gama);
betaS = radtodeg(beta);
betaStepeni = rad2deg(atan2(1.5*cosd(45)+1.5*sind(45), sqrt(6)/2));
disp(gamaS);
disp(betaS); % se dobivaat razlichni reshenija vo odnos na tgreshka kaj beta pak pishuva vo kniga 60stepeni
disp(r);
Tsph = [1 0 0 1.5; 0 1 0 1.5; 0 0 1 sqrt(6)/2; 0 0 0 1];
Tsph_vistinska = Tsph * troty(-pi/3) * trotz(-gamaS); % ima greshka pak
% iako probubuvam so pi/3 
%% 2.36
clc 
clear variables
syms T1 T2 T3
RotacijaZYZ = simplify(trotz(T1)*troty(T2)*trotz(T3)); % line#53
disp(RotacijaZYZ)
%% 2.37
clc 
clear variables
U = transl(3,0,0)*troty(pi/2)*trotz(-pi/2);
U1 = [0 0 1; -1 0 0; 0 -1 0];
disp(U1);
% euler = rotm2eul(U1, 'ZYZ'); ne mi raboti f-jata nemam licenca
Phi = atan(-U(2,3)/-U(1,3));
Psi = atan((-U(1,1)*sin(Phi)+U(2,1)*cos(Phi))/(-U(1,2)*sin(Phi)+U(2,2)*cos(Phi)));
Theta = atan((U(1,3)*cos(Phi)+U(2,3)*sin(Phi))/U(3,3));
PhiS = rad2deg(Phi);
disp(PhiS)
PsiS = rad2deg(Psi);
disp(PsiS);
ThetaS = rad2deg(Theta);
disp(ThetaS)
%% 2.38 Treba da se koristi rotm2eul ama ne mi raboti f-jata
clc
clear variables
U = trotx(pi/2)*trotz(pi/2)*transl(5,0,0)*troty(pi/4)*transl(0,0,sqrt(2));
disp(U)
Phi = atan(-U(2,3)/-U(1,3));
Psi = atan((-U(1,1)*sin(Phi)+U(2,1)*cos(Phi))/(-U(1,2)*sin(Phi)+U(2,2)*cos(Phi)));
Theta = atan((U(1,3)*cos(Phi)+U(2,3)*sin(Phi))/U(3,3));
PhiS = rad2deg(Phi);
disp(PhiS)
PsiS = rad2deg(Psi);
disp(PsiS);
ThetaS = rad2deg(Theta);
disp(ThetaS)
%% 2.39 
clc % Ojlerovi agli XYZ %%%POGLEDNI ZBIRKA%%%
clear variables
syms Phi Theta Psi
T = simplify(trotx(Phi)*troty(Theta)*trotz(Psi));
Psi = simplify(atan(T(1,2)/T(1,1)));
Phi = simplify(atan(T(2,3)/T(3,3)));
Theta = simplify(atan(T(1,2)/T(3,3)));
disp(Psi)
disp(Phi)
disp(Theta)
%% 2.40
clc 
clear variables 
% Ojlerovi agli XZY %%%VIDI ZBRIKA%%%
syms Phi Theta Psi
T = simplify(trotx(Phi)*trotz(Theta)*troty(Psi));
Psi = simplify(atan(T(1,3)/T(1,1)));
Phi = simplify(atan(T(3,2)/T(2,2)));
Theta = simplify(atan(T(1,2)/T(3,2)));
disp(Psi)
disp(Phi)
disp(Theta)
%% 2.41
clc
clear variables 
syms Phi Theta Ni
T = simplify(trotz(Phi)*troty(Theta)*trotx(Ni));
disp(T)
Phi = simplify(atan(T(2,1)/T(1,1)));
Theta = simplify(atan(T(3,1)/T(3,2)));
Ni = simplify(atan(T(3,2)/T(3,3)));
disp(Phi)
disp(Theta)
disp(Ni)
%% 
clc
clear variables
syms Phi Theta Ni
T = simplify(trotz(Phi)*troty(Theta)*trotx(Ni));
Tnov = subs(T, [cos(Theta) sin(Theta)], [0 1]);
Tv = simplify(Tnov);
disp(Tv) %% ne se sovpagja kako vo zbirka ima greshka 
%% 2.42
clc
clear vriables
B = trotx(pi/4)*trotz(pi/6)*transl(5,0,0)*troty(pi/3)*transl(0,0,3);
disp(B)
P = B(:,4);
disp(P)
Phi = atan(B(2,1)/B(1,1));
Theta = atan(-B(3,1)/(B(1,1)*cos(Phi)+B(2,1)*sin(Phi)));
Ni = atan((B(1,3)*sin(Phi)-B(2,3)*cos(Phi))/(-B(1,2)*sin(Phi)+B(2,2)*cos(Phi)));
PhiS = rad2deg(Phi);
ThetaS = rad2deg(Theta);
NiS = rad2deg(Ni);
disp(PhiS)
disp(ThetaS)
disp(NiS) %% greshka vo stepenite  +/- 6/7 stepeni
%% 2.43
clc
clear variables
B = [0.527 -0.574 0.628 4; 0.369 0.189 0.439 6; -0.766 0 0.643 9; 0 0 0 1];
Phi = atan(B(2,1)/B(1,1));
Theta = atan(-B(3,1)/(B(1,1)*cos(Phi)+B(2,1)*sin(Phi)));
Ni = atan((B(1,3)*sin(Phi)-B(2,3)*cos(Phi))/(-B(1,2)*sin(Phi)+B(2,2)*cos(Phi)));
PhiS = rad2deg(Phi);
ThetaS = rad2deg(Theta);
NiS = rad2deg(Ni);
disp(PhiS)
disp(ThetaS)
disp(NiS) %% Phi i Ni se dobri kaj Theta ima greshka 
% koga rabotam so tg treba i vtor agol +180
%% 2.44
clc
clear variables
% 2.43 za Ojlerovi
U = [0.527 -0.574 0.628 4; 0.369 0.189 0.439 6; -0.766 0 0.643 9; 0 0 0 1];
Phi = atan(-U(2,3)/-U(1,3));
Psi = atan((-U(1,1)*sin(Phi)+U(2,1)*cos(Phi))/(-U(1,2)*sin(Phi)+U(2,2)*cos(Phi)));
Theta = atan2((U(1,3)*cos(Phi)+U(2,3)*sin(Phi)),U(3,3));
PhiS = rad2deg(Phi);
disp(PhiS)
PsiS = rad2deg(Psi);
disp(PsiS);
ThetaS = rad2deg(Theta);
disp(ThetaS) %% pak greshka kaj Theta 
%% 2.45
clc
clear variables
% a)
Tph = eye(4);
Tph(3,3) = -1;
Trh = [0 -1 0 2; 1 0 0 -1; 0 0 1 0; 0 0 0 1] \ [1 0 0 1; 0 0 -1 4; 0 1 0 0; 0 0 0 1] * Tph;
disp(Trh)
% b)
syms r beta gama
Tsph = simplify(trotz(gama)*troty(beta)*transl(0,0,r));
disp(Tsph)
gama = atand(1/5);
% T(1,2) = sin(gama);  %vistnskata vrednost e 0 => ne se sferni koordinati 
% v)
% euler = rotm2eul([0 0 1; -1 0 0; 0 1 0]);
B = Trh;
Phi = atan(B(2,1)/B(1,1));
Theta = atan(-B(3,1)/(B(1,1)*cos(Phi)+B(2,1)*sin(Phi)));
Ni = atan((B(1,3)*sin(Phi)-B(2,3)*cos(Phi))/(-B(1,2)*sin(Phi)+B(2,2)*cos(Phi)));
PhiS = rad2deg(Phi);
ThetaS = rad2deg(Theta);
NiS = rad2deg(Ni);
disp(PhiS)
disp(ThetaS)
disp(NiS)
%% 2.46
clc 
clear variables
R01 = trotz(pi/2);
R12 = trotz(pi); % zashto ja vrtime za 180
R23 = trotz(-pi/2); % zashto ja vrakjame za -90
R03vidi = trotz(pi); % od kade ova
R03 = R01*R12*R23 ; 
disp(R03vidi);
disp(R03)
%% 2.47
% DH model
clc
clear variables
d1 = 2;
d2 = 2;
L(1) = Link([0 d1 0 -pi/2]);
L(2) = Link([0 d2 0 0]);
Robot = SerialLink(L);
Robot.plot([0 0])
syms d1 d2
T01 = trotz(0)*transl(0,0,d1)*corrp(transl(0,0,0)*trotx(-pi/2)); %f-ja za korekcija
T02 = trotz(0)*transl(0,0,d2)*transl(0,0,0)*trotx(0);
T02 = T01*T02;
disp(T02)
%% 2.48
clc
clear variables
d1 = 2;
d2 = 2;
d3 = 2;
L(1) = Link([0 d1 0 -pi/2]);
L(2) = Link([0 d2 0 pi/2]);
L(3) = Link([0 d3 0 0]);
Robot = SerialLink(L);
Robot.plot([0 0 0])
syms d1 d2 d3
A1 =  trotz(0)*transl(0,0,d1)*corrp(transl(0,0,0)*trotx(-pi/2));
disp(A1)
A2 =  corrp(trotz(pi/2))*transl(0,0,d2)*corrp(transl(0,0,0)*trotx(pi/2));
disp(A2)
A3 =  trotz(0)*transl(0,0,d3)*corrp(transl(0,0,0)*trotx(0));
disp(A3)
T03 = A1*A2*A3;
disp(T03) 
%% 2.49
clc
clear variables
Theta1 = pi/6;
Theta2 = pi/6;
l1 = 3;
l2 = 4;
L(1) = Link([Theta1 0 l1 0]);
L(2) = Link([Theta2 0 l2 -pi/2]); % mesto 0 tuka stavam -pi/2 za da se sovpadne so slikata
Robot = SerialLink(L);
Robot.plot([0 -pi/3]) % spored ova Z-oskata na slikata e nagore 
% spored zbirkata Z-oskata treba da bide legnata
syms Theta1 Theta2 L1 L2
A1 =  trotz(Theta1)*transl(0,0,0)*transl(L1,0,0)*trotx(0);
A2 =  trotz(Theta2)*transl(0,0,0)*transl(L2,0,0)*corrp(trotx(-pi/2));
A = A1*A2;
disp(simplify(A))
%% 2.50
clc
clear variables
Theta1 = 0;
Theta2 = 0;
Theta3 = 0;
L1 = 1;
L2 = 1;
L3 = 1;
L(1) = Link([Theta1 0 L1 0]);
L(2) = Link([Theta2 0 L2 0]);
L(3) = Link([Theta3 0 L3 0]);
Robot = SerialLink(L);
Robot.plot([pi/3 pi/3 pi/3]);
syms Theta1 Theta2 Theta3 L1 L2 L3
A1 = trotz(Theta1)*transl(0,0,0)*transl(L1,0,0)*trotx(0);
A2 = trotz(Theta2)*transl(0,0,0)*transl(L2,0,0)*trotx(0);
A3 = trotz(Theta3)*transl(0,0,0)*transl(L3,0,0)*trotx(0);
A = A1*A2*A3;
disp(simplify(A));
%% 2.51
clc
clear variables
Theta1 = 0;
Theta2 = 0;
Theta3 = 0;
D1 = 3;
L2 = 1;
L3 = 1;
L(1) = Link([Theta1 D1 0 pi/2]);
L(2) = Link([Theta2 0 L2 0]);
L(3) = Link([Theta3 0 L3 0]);
Robot = SerialLink(L);
Robot.plot([0 0 0])
syms Theta1 Theta2 Theta3 D1 L2 L3
A1 = trotz(Theta1)*transl(0,0,D1)*transl(0,0,0)*corrp(trotx(pi/2));
A2 = trotz(Theta2)*transl(0,0,0)*transl(L2,0,0)*trotx(0);
A3 = trotz(Theta3)*transl(0,0,0)*transl(L3,0,0)*trotx(0);
A = A1*A2*A3;
disp(simplify(A)); % D1 = 0 ili ima rastojanie vo zbirkata 
% vo krajnata vrednost ne stoi
%% 2.52
clc
clear variables
Theta1 = 0;
Theta3 = 0;
D2 = 3;
L3 = 1;
L(1) = Link([Theta1 3 0 -pi/2]);
L(2) = Link([0 D2 0 pi/2]);
L(3) = Link([Theta3 0 L3 0]);
Robot = SerialLink(L);
Robot.plot([0 pi/3 0])
syms Theta1 Theta2 Theta3 D2 L2 L3
A1 = trotz(Theta1)*transl(0,0,0)*transl(0,0,0)*corrp(trotx(pi/2));
A2 = trotz(0)*transl(0,0,D2)*transl(0,0,0)*corrp(trotx(-pi/2));
A3 = trotz(Theta3)*transl(0,0,0)*transl(L3,0,0)*trotx(0);
A = A1*A2*A3;
disp(simplify(A));
%% 2.53
clc 
clear variables
Theta1 = 0;
D1 = 3;
D2 = 2;
D3 = 2;
L(1) = Link([Theta1 D1 0 0]);
L(2) = Link([0 D3 0 -pi/2]);
L(3) = Link([0 D3 0 0]);
Robot = SerialLink(L);
Robot.plot([0 0 0])
syms Theta1 D1 D2 D3
A1 = trotz(Theta1)*transl(0,0,D1)*transl(0,0,0)*corrp(trotx(0));
A2 = trotz(0)*transl(0,0,D2)*transl(0,0,0)*corrp(trotx(-pi/2));
A3 = trotz(0)*transl(0,0,D3)*transl(0,0,0)*trotx(0);
A = A1*A2*A3;
disp(simplify(A));
%% 2.54
% kade e D6 ?? greshka
clc
clear variables
Theta4 = 0;
Theta5 = 0;
Theta6 = 0;
D5 = 3;
D6 = 3;
L(1) = Link([Theta4 0 0 -pi/2]); 
L(2) = Link([Theta5 D5 0 pi/2]); 
L(3) = Link([Theta6 D6 0 0]);
Robot = SerialLink(L);
Robot.plot([0 0 0])
syms Theta4 Theta5 Theta6 D6 L2 L3
A1 = trotz(Theta4)*transl(0,0,0)*transl(0,0,0)*corrp(trotx(-pi/2));
A2 = trotz(Theta5)*transl(0,0,0)*transl(0,0,0)*corrp(trotx(pi/2));
A3 = trotz(Theta6)*transl(0,0,D6)*transl(0,0,0)*trotx(0);
A = A1*A2*A3;
disp(simplify(A));
%% 2.55
clc 
clear variables
syms Theta1 Theta2 D2 D3
A1 = trotz(Theta1)*transl(0,0,0)*transl(0,0,0)*corrp(trotx(-pi/2));
A2 = trotz(Theta2)*transl(0,0,D2)*transl(0,0,0)*corrp(trotx(pi/2));
A3 = trotz(0)*transl(0,0,D3)*transl(0,0,0)*trotx(0);
A = A1*A2*A3;
disp(simplify(A));
Theta1 = 0;
Theta2 = 0;
D2 = 3;
D3 = 1;
L(1) = Link([Theta1 0 0 -pi/2]);
L(2) = Link([Theta2 D2 0 pi/2]);
L(3) = Link([0 0 D3 0]);
Robot = SerialLink(L);
Robot.plot([0 0 0])
%% 2.56
% vo DH modelot ne e vkluchen 4tiot zglob rotacionen
clc 
clear variables
Theta1 = 0;
Theta2 = 0;
Theta4 = 0;
a1 = 2;
a2 = 2;
d3 = 4;
L(1) = Link([Theta1 0 a1 0]);
L(2) = Link([Theta2 0 a2 -pi]);
L(3) = Link([0 d3 0 0]);
L(4) = Link([Theta4 0 0 0]);
Robot = SerialLink(L);
Robot.plot([0 0 0 0]);
syms Theta1 Theta2 Theta4 a1 a2 d3
A1 = trotz(Theta1)*transl(0,0,0)*transl(a1,0,0)*corrp(trotx(0));
A2 = trotz(Theta2)*transl(0,0,0)*transl(a2,0,0)*corrp(trotx(-pi));
A3 = trotz(0)*transl(0,0,d3)*transl(0,0,0)*trotx(0);
A = A1*A2*A3;
disp(simplify(A))
A4 = trotz(Theta4);
A = A*A4;
disp(simplify(A)) % razlichno e od zbirkata ama tochno e 
%% 2.57
clc 
clear variables
Theta1 = 0;
Theta2 = 0;
d1 = 2;
d2 = 2;
d3 = 4;
a2 = 2;
L(1) = Link([Theta1 d1 0 pi/2]);
L(2) = Link([Theta2 d2 a2 pi/2]);
L(3) = Link([0 d3 0 0]);
Robot = SerialLink(L);
Robot.plot([0 0 0]);
syms Theta1 Theta2 d1 d2 d3 a2
A1 = trotz(Theta1)*transl(0,0,0)*transl(0,0,0)*corrp(trotx(pi/2));
A2 = trotz(Theta2)*transl(0,0,0)*transl(a2,0,0)*corrp(trotx(pi/2));
A3 = trotz(0)*transl(0,0,d3)*transl(0,0,0)*trotx(0);
A = A1*A2*A3;
disp(simplify(A))
%% 2.58
clc
clear variables
Theta2 = 0;
Theta3 = 0;
d1 = 2;
d4 = 2;
L3 = 4;
L2 = 4;
L(1) = Link([0 d1 0 0]);
L(2) = Link([Theta2 0 L2 0]);
L(3) = Link([Theta3 0 L3 pi]); % alpha e nula
L(4) = Link([0 d4 0 0]); % alpha e pi vo zbirka greshka
Robot = SerialLink(L);
Robot.plot([0 0 0 0]);
syms Theta2 Theta3 L2 L3 d1 d4
A1 = trotz(0)*transl(0,0,d1)*transl(0,0,0)*corrp(trotx(0));
A2 = trotz(Theta2)*transl(0,0,0)*transl(L2,0,0)*corrp(trotx(0));
A3 = trotz(Theta3)*transl(0,0,0)*transl(L3,0,0)*corrp(trotx(pi));
A4 = trotz(0)*transl(0,0,d4)*transl(0,0,0)*trotx(0);
A = A1*A2*A3*A4;
disp(simplify(A)) % se dobiva skoro ista matrica kako vo knigata
%% 2.59
clc
clear variables
syms Theta1 D1 D2 D3
A1 = trotz(Theta1)*transl(0,0,D1)*transl(0,0,0)*corrp(trotx(0));
A2 = trotz(0)*transl(0,0,D2)*transl(0,0,0)*corrp(trotx(-pi/2));
A3 = trotz(0)*transl(0,0,D3)*transl(0,0,0)*trotx(0);
A = A1*A2*A3;
T03 = A;
disp(T03)
syms Theta4 Theta5 Theta6 D6 L2 L3
A1 = trotz(Theta4)*transl(0,0,0)*transl(0,0,0)*corrp(trotx(-pi/2));
A2 = trotz(Theta5)*transl(0,0,0)*transl(0,0,0)*corrp(trotx(pi/2));
A3 = trotz(Theta6)*transl(0,0,D6)*transl(0,0,0)*trotx(0);
A = A1*A2*A3;
T36 = A;
disp(T36)
T06 = T03*T36;
disp(simplify(T06))
%% 2.60
clc
clear variables
syms Theta1 Theta2 L1 L2
A1 = trotz(Theta1)*transl(0,0,0)*transl(L1,0,0)*corrp(trotx(0));
A2 = trotz(Theta2)*transl(0,0,0)*transl(L2,0,0)*corrp(trotx(-pi/2));
A = A1*A2;
disp(simplify(A))
temp1 = A(:,2);
temp1(3,1) = 1;
temp2 = A(:,3);
Toh = [A(:,1) temp2 temp1 A(:,4)];
Toh = simplify(Toh);
Toh = subs(Toh, [L1 L2], [1 1]);
Toh = subs(Toh, [cos(Theta1 + Theta2) sin(Theta1 + Theta2)], [-0.2924 0.9563]);
Theta1 = atan2((0.6978+731/2500),(0.8172+9563/10000));
disp(rad2deg(Theta1))
Theta2 = atan2(-0.9563,0.2824);
disp(rad2deg(Theta2)) % greshka vo zbirka ne ispagjaat aglite taka
%% 2.61
clc
clear variables
L(1) = Link([0 3 0 -pi/2]);
L(2) = Link([0 0 0 pi/2]);
L(3) = Link([0 3 0 0]);
Robot = SerialLink(L);
Robot.plot([0 0 0])
syms Theta1 Theta2 d1 d3
A1 = trotz(Theta1)*transl(0,0,d1)*transl(0,0,0)*corrp(trotx(-pi/2));
A2 = trotz(Theta2)*transl(0,0,0)*transl(0,0,0)*corrp(trotx(pi/2));
A3 = trotz(0)*transl(0,0,d3)*transl(0,0,0)*corrp(trotx(0));
A = A1*A2*A3;
disp(simplify(A))
x = A(1,4);
disp(x)
y = A(2,4);
disp(y)
z = A(3,4);
disp(z)
%% 2.62
clc
clear variables
L(1) = Link([0 3 0 -pi/2]);
L(2) = Link([0 3 0 pi/2]);
L(3) = Link([0 0 0 0]);
Robot = SerialLink(L);
Robot.plot([0 0 0])
syms Theta1 Theta2 d1 d2 d3
A1 = trotz(Theta1)*transl(0,0,d1)*transl(0,0,0)*corrp(trotx(-pi/2));
A2 = trotz(Theta2)*transl(0,0,d2)*transl(0,0,0)*corrp(trotx(pi/2));
A3 = trotz(0)*transl(0,0,d3)*transl(0,0,0)*corrp(trotx(0));
A = A1*A2*A3;
disp(simplify(A))
x = A(1,4);
disp(x)% da prasham zasho taka se izrazeni x,y,z
y = A(2,4);
disp(y)
z = A(3,4);
disp(z)
%% 2.63
clc
clear variables
L(1) = Link([0 3 0 pi/2]);
L(2) = Link([0 0 3 -pi/2]);
L(3) = Link([0 0 3 0]);
Robot = SerialLink(L);
Robot.plot([0 0 0])
syms Theta3 Theta2 d1 a3 a2
A1 = trotz(0)*transl(0,0,d1)*transl(0,0,0)*corrp(trotx(pi/2));
A2 = trotz(Theta2)*transl(0,0,0)*transl(a2,0,0)*corrp(trotx(-pi/2));
A3 = trotz(Theta3)*transl(0,0,0)*transl(a3,0,0)*corrp(trotx(0));
A = A1*A2*A3;
disp(simplify(A))
x = A(1,4);
y = A(2,4);
z = A(3,4);
disp(x);
disp(y);
disp(z)
%% 2.64
clc
clear variables
syms Theta1 Theta2 Theta3 l1 l2 l3 
A1 = trotz(Theta1)*transl(0,0,0)*transl(l1,0,0)*corrp(trotx(-pi/2));
A2 = trotz(Theta2)*transl(0,0,0)*transl(l1,0,0)*corrp(trotx(pi/2));
A3 = trotz(Theta3)*transl(0,0,0)*transl(l3,0,0)*corrp(trotx(-pi/2));
A = A1*A2*A3;
disp(simplify(A))
disp(subs(A, [l1 l2 l3], [1 1 1]))
A = subs(A, [l1 l2 l3], [1 1 1]);
Theta1 = atan((A(2,2)/A(1,2)));
Theta3 = atan((A(3,3)/A(3,1)));% dali mozhe vaka i posle Theta2
% da se najde od nekoe drugo
disp(Theta1);
disp(Theta3)
%% 2.65
clc
clear variables
syms Theta1 Theta2 d1 d2 d3
A1 = trotz(Theta1)*transl(0,0,0)*transl(0,0,0)*corrp(trotx(-pi/2));
A2 = trotz(Theta2)*transl(0,0,d2)*transl(0,0,0)*corrp(trotx(pi/2));
A3 = trotz(0)*transl(0,0,d3)*transl(0,0,0)*corrp(trotx(0));
A = A1*A2*A3;
disp(simplify(A))
Theta1 = atan(A(1,2)/A(2,2));
disp(Theta1)
Theta2 = atan(A(3,1)/A(3,3));
disp(Theta2)
d3 = A(3,4)/A(3,3);
disp(d3);
%% 2.66
clc
clear variables
syms Theta1 d2 d3
A1 = trotz(Theta1)*transl(0,0,1)*transl(0,0,0)*corrp(trotx(0));
A2 = trotz(0)*transl(0,0,d2)*transl(1,0,0)*corrp(trotx(-pi/2));
A3 = trotz(0)*transl(0,0,d3)*transl(0,0,0)*corrp(trotx(0));
A = A1*A2*A3;
disp(simplify(A))
A0vrednosti = subs(A, [Theta1 d2 d3], [0 0 0]); % posledna kolona 0 1 1 1 greshka
disp(A0vrednosti)
Tnova = A0vrednosti*corrp(trotz(pi/2));
disp(Tnova)
Theta1 = atan2(-A(1,4),A(2,4)); % neshto ne e vo red razlichni e A;
disp(Theta1)
%% DIFERENCIJALNI DVIZHENJA I BRZINI
%% Voved
% pod poimot dif. dvizhenja se podrazbiraat mn mali dvizhenja
% mali dvizhenja, za mal vremenski interval dt
% D = J*Dq
% D = [dx; dy; dz; deltax; deltay; deltaz]
% Dq = [dq1; dq2; dq3; dq4; dq5; dq6]
% J - jakobijan 6x6 ili nx6 stepeni na sloboda
% ako D i Dq se podelat so dt
% v = J*q_tocka
% v - brzina, q_tocka - zglobovi
% J - ja definira vrskata pomegju diferencijalnite dvizhenja i brzinite
% na oddelnite zglobovi na rakata
%% Diferencijalna translacija
clc 
clear variables
syms dx dy dz
TransDiff = transl(dx,dy,dz);
disp(TransDiff)
%% Diferencijalna rotacija 
clc
clear variables
syms deltax deltay deltaz

RotXDiff = trotx(deltax);
RotXDiff = subs(RotXDiff, [cos(deltax) sin(deltax)], [0 deltax]);
RotXDiff(2,2) = 1;
RotXDiff(3,3) = 1;
disp(RotXDiff);

RotYDiff = troty(deltay);
RotYDiff = subs(RotYDiff, [cos(deltay) sin(deltay)], [0 deltay]);
RotYDiff(1,1) = 1;
RotYDiff(3,3) = 1;
disp(RotYDiff);

RotZDiff = trotx(deltaz);
RotZDiff = subs(RotZDiff, [cos(deltaz) sin(deltaz)], [0 deltaz]);
RotZDiff(2,2) = 1;
RotZDiff(3,3) = 1;
disp(RotZDiff);

RotK_po_Theta = [1 -deltaz deltay 0
                 deltaz 1 -deltax 0
                 -deltay deltax 1 0
                 0 0 0 1];
disp(RotK_po_Theta)
%% Vkupna diferencijalna transformacija
% T + dT = [Trans(dx,dy,dz)*R(k,dTheta)]*T
% dT = triangleM*T
clc
clear variables
syms dx dy dz deltax deltaz deltay
RotK_po_Theta = [1 -deltaz deltay 0
                 deltaz 1 -deltax 0
                 -deltay deltax 1 0
                 0 0 0 1];
triangleM = transl(dx,dy,dz)*RotK_po_Theta - eye(4);
disp(triangleM) %diferencijalen operator
%% Vektor na linearna brzina i vektor na aglova brzina
% v0n = Jv*q_tocka
% w0n = Jw*q_tocka
% [v0n; w0n] = [Jv; Jw]*q_tocka
% Jvi = diff(Pn)/diff(qi), i = 1,2,3,4...
% Jv = [Jv1 Jv2 Jv3 ...]
% Jw = [Jw1 Jw2 Jw3 ...] = [ro1*z ro2*z ro3*z ...]
% ako e translatoren ro = 0, ako e rotacionen ro = 1
% v = [J1 J2 J3 ...]*[q1_tocka; q2_rocka; q3_tocka; ...]
%% Singulariteti
% rangor na J na edna robotska raka zavisi od nejzinata konfiguracija q
% ako rang(J(q)) <= min(6,n) singulariteti ili singularni konfiguracii
% singulariteti se odreduvaat od uslovot za |J| = det(J) = 0;
%% ZADACHI TEMA 3
%% 3.1
clc 
clear variables
triangleM = F_triangle(0.1,0.1,0.3,0,0,0.15);
T = [0 0 1 2; 1 0 0 7; 0 1 0 5; 0 0 0 1];
deltaT = triangleM*T;
Tnova = T + deltaT;
disp(Tnova)
%% 3.2
clc
clear variables
T = [0 0 1 4; 1 0 0 2; 0 1 0 1; 0 0 0 1];
Tnova = T + F_triangle(0.05,0.02,0.01,0,0,0.15)*T;
disp(Tnova)
dT = F_triangle(0.05,0.02,0.01,0,0,0.15)*T;
disp(dT)
%% 3.3
clc
clear variables
T = [1 0 0 5; 0 0 1 3; 0 -1 0 8; 0 0 0 1];
dT = [0 -0.1 -0.1 0.6; 0.1 0 0 0.5; -0.1 0 0 -0.5; 0 0 0 0];
triangleM = dT / T; 
disp(triangleM)
syms dx dy dz deltax deltay deltaz 
disp(F_triangle(dx,dy,dz,deltax,deltay,deltaz));
triangleM_T = T \ triangleM * T;
disp(triangleM_T)
%% 3.4
clc
clear variables 
A = [0 0 1 10; 1 0 0 5; 0 1 0 0; 0 0 0 1];
triangleM = F_triangle(1,0,0.5,0,0.1,0);
disp(triangleM)
triangleM_A = A \ triangleM * A;
disp(triangleM_A)
%% 3.5
clc
clear variables
J = [8 0 0 0 0 0;
    -3 0 1 0 0 0;
    0 10 0 0 0 0;
     0 1 0 0 1 0;
     0 0 0 1 0 0;
    -1 0 0 0 0 1];
T = [0 1 0 10; 1 0 0 5; 0 0 -1 0; 0 0 0 1];
Dtheta = [0;0.1;-0.1;0.2;0.2;0];
D = J*Dtheta;
disp(D)
triangleM = F_triangle(0,-0.1,1,0.3,0.2,0);
disp(triangleM)
dT = triangleM*T;
disp(dT)
Tnova = T+dT;
disp(Tnova) % greshka kaj 0.5 vo zbirka -0.5
%% 3.6
clc 
clear variables
T = [0 0 1 1; 1 0 0 5; 0 1 0 0; 0 0 0 1];
triangleM = F_triangle(0.5,0,1,0,0.2,0);
dT = triangleM * T;
Tnova = T + dT;
disp(Tnova)
triangleM_T = T \ triangleM * T ;
disp(triangleM_T) % greshka na (3,3) vo zbirkata stoi 1
%% 3.7
clc
clear variables

T1 = [1 0 0 5; 0 0 -1 3; 0 1 0 6; 0 0 0 1]; % pochetna poz
T2 = [1 0 0.1 4.8; 0.1 0 -1 3.5; 0 1 0 6.2; 0 0 0 1]; %nova poz
% T2 = Q*T1
Q = T2 / T1;
disp(Q)
% T2 = T1 + dT
dT = T2 - T1;
disp(dT)
% dT = triangleM*T1
triangleM = dT / T1;
disp(triangleM)
% vrednosti
D = [0.1; 0; 0.2; 0; 0; 0.1];
disp(D)
%% 3.8
clc
clear variables
syms alpha r L
Tcyl = transl(0,0,L) * trotz(alpha) * transl(r,0,0);
disp(Tcyl)

Jv = jacobian([r*cos(alpha), r*sin(alpha), L],[r alpha L]);
pretty(Jv)

syms x(t) y(t) z(t) alpha(t) r(t) L(t)

x_tocka = diff(r*cos(alpha), t);
pretty(x_tocka)

y_tocka = diff(r*sin(alpha), t);
pretty(y_tocka)

z_tocka = diff(L, t);
pretty(z_tocka)

%% 3.9
clc
clear variables
syms r beta gama
T = simplify(trotz(gama)*troty(beta)*transl(0,0,r));
pretty(T)

Jv = jacobian([r*cos(gama)*sin(beta), r*sin(beta)*sin(gama), r*cos(beta)], [r beta gama]);
pretty(Jv)              

syms r(t) beta(t) gama(t)

x_tocka = diff(r*cos(gama)*sin(beta), t);
pretty(simplify(x_tocka))

y_tocka = diff(r*sin(beta)*sin(gama), t);
pretty(simplify(y_tocka))

z_tocka = diff(r*cos(beta), t);
pretty(simplify(z_tocka))

detJv = det(Jv);
pretty(simplify(detJv))

%% 3.10
clc 
clear variables
syms d1 d2 Theta3 a3
d1 = 1; 
d2 = 1;
Theta3 = 0;
a3 = 1;
L(1) = Link([0 d1 0 -pi/2],'standard');
L(2) = Link([0 d2 0 pi/2],'standards');
L(3) = Link([Theta3 0 a3 0],'standard');
Robot = SerialLink(L);
Robot.name = 'biki';
Robot.plot([0 0 0]);
syms d1 d2 Theta3 a3
A1 = trotz(0)*transl(0,0,d1)*transl(0,0,0)*corrp(trotx(-pi/2));
A2 = trotz(0)*transl(0,0,d2)*transl(0,0,0)*corrp(trotx(pi/2));
A3 = trotz(Theta3)*transl(0,0,0)*transl(a3,0,0)*trotx(0);
T = simplify(A1 * A2 * A3);
pretty(T)
Jv = jacobian([a3*cos(Theta3), d2 + 1.0*a3*sin(Theta3), d1], [d1 d2 Theta3]);
pretty(Jv)
Jw = [0 0 0; 0 0 0; 0 0 1]; %tretiot zglob e rotacionen
J = [Jv; Jw];
pretty(J)

syms d1(t) d2(t) Theta3(t) a3

x_tocka = diff(a3*cos(Theta3), t);
pretty(x_tocka)

y_tocka = diff(a3*sin(Theta3)+d2, t);
pretty(y_tocka)

z_tocka = diff(d1, t);
pretty(z_tocka)
%% 3.11
clc
clear variables
syms Theta1 a1 d2
T01 = [cos(Theta1) 0 -sin(Theta1) a1*cos(Theta1);
       sin(Theta1) 0 cos(Theta1) a1*sin(Theta1);
       0 -1 0 0;
       0 0 0 1];
disp(T01);
T02 = [cos(Theta1) 0 -sin(Theta1) a1*cos(Theta1)-d2*sin(Theta1);
       sin(Theta1) 0 cos(Theta1) a1*sin(Theta1)+d2*cos(Theta1);
       0 -1 0 0;
       0 0 0 1];
Jv = jacobian([T02(1,4) T02(2,4) 0], [Theta1 d2]);
pretty(Jv)
Jw = [0 0; 0 0; 1 0];
J = [Jv; Jw];
pretty(J)
%% 3.12
clc
clear variables
Theta1 = 0;
Theta2 = 0;
l1 = 3;
l2 = 3;

L(1) = Link([Theta1 0 l1 0]);
L(2) = Link([Theta2 0 l2 0]);
Robot = SerialLink(L);
Robot.plot([0 0])

syms Theta1 Theta2 l1 l2
A1 = trotz(Theta1)*transl(0,0,0)*transl(l1,0,0)*trotx(0);
A2 = trotz(Theta2)*transl(0,0,0)*transl(l2,0,0)*trotx(0);
A = A1*A2;
A = simplify(A);
disp(A)

Jv = jacobian([A(1,4) A(2,4) A(3,4)], [Theta1 Theta2]);
Jw = [0 0; 0 0; 1 1];
J = [Jv; Jw];
disp(J)

%% 3.13
clc
clear variables
syms Theta2 Theta3 d1 a2 a3
A1 = trotz(0)*transl(0,0,d1)*transl(0,0,0)*corrp(trotx(pi/2));
A2 = trotz(Theta2)*transl(0,0,0)*transl(a2,0,0)*corrp(trotx(-pi/2));
A3 = trotz(Theta3)*transl(0,0,0)*transl(a3,0,0)*trotx(0);
A = simplify(A1*A2*A3);
disp(A)

Theta2 = 0;
Theta3 = 0;
d1 = 2;
a2 = 2;
a3 = 2;
L(1) = Link([0 d1 0 pi/2]);
L(2) = Link([Theta2 0 a2 -pi/2]);
L(3) = Link([Theta3 0 a3 0]);
Robot = SerialLink(L);
Robot.plot([0 0 0]);

syms Theta2 Theta3 d1 a2 a3
Jv = jacobian([cos(Theta2)*(a2 + a3*cos(Theta3)), a3*sin(Theta3), d1 + a2*sin(Theta2) + a3*cos(Theta3)*sin(Theta2)],[d1 Theta2 Theta3]);
Jw = [0 0 -sin(Theta2)
      0 -1 cos(Theta2)
      0 0 0];
J = [Jv; Jw];
disp(J) % ne se sovpagja so Jacobian-ot vo knigata greshka
%% 3.14
clc
clear variables
syms Theta1 Theta2 Theta3 L1 L2 L3
A1 = trotz(Theta1)*transl(0,0,0)*transl(L1,0,0)*trotx(0);
A2 = trotz(Theta2)*transl(0,0,0)*transl(L2,0,0)*trotx(0);
A3 = trotz(Theta3)*transl(0,0,0)*transl(L3,0,0)*trotx(0);
T03 = A1 * A2 * A3;
T03 = simplify(T03);
disp(T03);
T02 = A1 * A2;
disp(simplify(T02))
T02 = simplify(T02);
disp(T02)
T01 = A1;
disp(simplify(T01))
jv = jacobian([L2*cos(Theta1 + Theta2) + L1*cos(Theta1) + L3*cos(Theta1 + Theta2 + Theta3), L2*sin(Theta1 + Theta2) + L1*sin(Theta1) + L3*sin(Theta1 + Theta2 + Theta3), 0], [Theta1 Theta2 Theta3]);
jv = simplify(jv);
jw = [0 0 0; 0 0 0; 1 1 1];
J = [jv; jw];
disp(J)
%% 3.15
clc
clear variables

Theta1 = 0;
Theta2 = 0;
Theta3 = 0;
d1 = 2;
d2 = 2;
d3 = 2;
L(1) = Link([Theta1 0 0 pi/2]);
L(2) = Link([Theta2 0 0 pi/2]);
L(3) = Link([Theta3 d3 0 0]);
Robot = SerialLink(L);
Robot.plot([0 0 0]); % ne e dobar dh modelot i ne se sovpagja so slikata 
% istava zadacha ja ima i prethodno vo zbirkata so drug DH model

syms Theta1 Theta2 Theta3 d1 d2 d3
A1 = trotz(Theta1)*transl(0,0,0)*transl(0,0,0)*corrp(trotx(pi/2));
A2 = trotz(Theta2)*transl(0,0,0)*transl(0,0,0)*corrp(trotx(pi/2));
A3 = trotz(Theta3)*transl(0,0,d3)*transl(0,0,0)*trotx(0);
A = simplify(A1*A2*A3);
disp(A) % ne e mozhno d2 da se pojavuva vo izrazot
disp(simplify(A1*A2))
disp(simplify(A1))
Jv = jacobian([d3*cos(Theta1)*sin(Theta2), d3*sin(Theta1)*sin(Theta2), -d3*cos(Theta2)],[Theta1 Theta2 d3]);
Jw = [0 sin(Theta1) sin(Theta1)
      0 -cos(Theta1) -cos(Theta1)
      1 0 0];
J = [Jv; Jw];
disp(J)%zadachata e greshna
%% 3.16
clc
clear variables
syms Theta2 Theta3 L2 L3 D1

A1 = trotz(0)*transl(0,0,D1)*transl(0,0,0)*corrp(trotx(-pi/2));
A2 = trotz(Theta2)*transl(0,0,0)*transl(-L2,0,0)*corrp(trotx(0));
A3 = trotz(Theta3)*transl(0,0,0)*transl(-L3,0,0)*corrp(trotx(0));
A = simplify(A1*A2*A3);
disp(A)

syms Theta2(t) Theta3(t) L2 L3 D1(t)
x = -L2*sin(Theta2)-L3*sin(Theta2+Theta3);
z = D1+L2*cos(Theta2)+L3*cos(Theta2+Theta3);

x_tocka = diff(x, t);
z_tocka = diff(z, t);

pretty(x_tocka)
pretty(z_tocka)

syms Theta2 Theta3 L2 L3 D1
x = -L2*sin(Theta2)-L3*sin(Theta2+Theta3);
z = D1+L2*cos(Theta2)+L3*cos(Theta2+Theta3);
alpha = Theta2+Theta3;
Jv = jacobian([x, z, alpha], [D1 Theta2 Theta3]);
disp(Jv)

detJv = simplify(det(Jv));
disp(detJv)
detJv = subs(detJv, L2, 1);
% od tuka sleduva za pi/2
%% 3.17
clc
clear variables
Theta1 = 0;
Theta2 = 0;
Theta3 = 0;
L1 = 1;
L2 = 1;
L3 = 1;

L(1) = Link([Theta1 0 L1 pi/2]);
L(2) = Link([Theta2 0 L2 -pi/2]);
L(3) = Link([Theta3 0 L3 pi/2]);
Robot = SerialLink(L);
Robot.plot([0 0 0]) %vaka e spored slikata, ama ne spored zbirkata DH greshka

syms Theta1 Theta2 Theta3 L1 L2 L3
 
A1 = trotz(Theta1)*transl(0,0,0)*transl(L1,0,0)*corrp(trotx(pi/2));
A2 = trotz(Theta2)*transl(0,0,0)*transl(L2,0,0)*corrp(trotx(-pi/2));
A3 = trotz(Theta3)*transl(0,0,0)*transl(L3,0,0)*corrp(trotx(pi/2));
A = simplify(A1*A2*A3);
A21 = simplify(A1*A2);
% disp(A); %matricata za dir kinematika se sovpagja so knigata
A = subs(A, [L1 L2 L3], [1 1 1]);
disp(A)

Jv = jacobian([A(1,4) A(2,4) A(3,4)], [Theta1 Theta2 Theta3]);
Jw = [0 A1(1,3) A21(1,3); 0 A1(2,3) A21(2,3); 1 A1(3,3) A21(3,3)];

J = [Jv; Jw];
disp(J) % neshto ne e vo red so znacite

% da se najde Jv ama ne vo odnos na referenten koordinaten s-m tuku 
% vo odnos na 1 koordinatesn s-m posle prviot zglob

R = trotz(Theta1);
R33 = [cos(Theta1), -sin(Theta1), 0; sin(Theta1),  cos(Theta1), 0; 0 0 1];
Jv1 = R33 \ Jv;
disp(simplify(Jv1)); 

% zashto barame singulariteti za Jv1

Jv1det = det(Jv1);
disp(simplify(Jv1det)) % reshenija se Theta3 = +/-pi ili 0
% Theta2 = +/-pi
%% 3.18
syms L1 L2 Theta2 d1
x = L1 + L2*cos(Theta2);
z = d1 + L2*sin(Theta2); % mesto L3 treba da stoi L2
y = 0;
Jv = jacobian([x y z], [d1 Theta2]);
disp(Jv)
% singulariteti
Jvdet = det([Jv(1,:); Jv(3,:)]);
disp(Jvdet) % 0 i +/-pi
%% 3.19
clc
clear variables
% kade e d4 na slikata ??
 Theta1 = 0;
 Theta2 = 0;
 Theta4 = 0;
 D3 = 3;
 D4 = 3; % ne moze da postoi D4 kje treba da ima prodolzhuvanje na rakata
 a1 = 3;
 a2 = 3;
 L(1) = Link([Theta1 0 a1 0]);
 L(2) = Link([Theta2 0 a2 pi]);
 L(3) = Link([0 D3 0 0]);
 L(4) = Link([Theta4 D4 0 0]);
Robot = SerialLink(L);
Robot.plot([0 0 0 0])
syms Theta1 Theta2 Theta4 D3 D4 a1 a2
A1 = trotz(Theta1)*transl(0,0,0)*transl(a1,0,0)*corrp(trotx(0));
A2 = trotz(Theta2)*transl(0,0,0)*transl(a2,0,0)*corrp(trotx(pi));
A3 = trotz(0)*transl(0,0,D3)*transl(0,0,0)*corrp(trotx(0));
A4 = trotz(Theta4)*transl(0,0,D4)*transl(0,0,0)*corrp(trotx(0));
A = simplify(A1*A2*A3*A4);
A12 = A1*A2;
A123 = A12*A3;
disp(A);

Jv = jacobian([A(1,4) A(2,4) A(3,4)], [Theta1 Theta2 D3 Theta4]);
Jw = [[0; 0; 1] A12(1:3,3) [0; 0; 0] A(1:3,3)];
J = [Jv; Jw];% se dobiva razlichen jakobijan
disp(J)

Jdet = det([J(1,:); J(2,:); J(3,:); J(6,:)]);
simplify(Jdet) %se dobiva 0 i pi isto kako vo zbirkata

%% 3.20
clc
clear variables
syms d1 Theta2 Theta3 d4 l2 l3
T1 = [1 0 0 0; 0 1 0 0; 0 0 1 d1; 0 0 0 1];
T2 = trotz(Theta2);
T3 = trotz(Theta3);
T3(1,4) = l2;
T4 = transl(l3,0,d4);
T = simplify(T1*T2*T3*T4);
disp(T)
Jv = jacobian([T(1,4) T(2,4) T(3,4)], [d1 Theta2 Theta3 d4]);
disp(Jv)
%% 3.21
clc
clear variables

syms Theta1 Theta2 L1 L2
A1 = trotz(Theta1)*transl(L1,0,0);
A2 = trotz(Theta2)*transl(L2,0,0);
A = simplify(A1*A2);
disp(A);
Jv = jacobian([A(1,4) A(2,4) A(3,4)], [Theta1 Theta2]);
Jvdet = det([Jv(1,:); Jv(2,:)]);
Jvdet = simplify(Jvdet);
disp(Jvdet)
% 0 i pi

syms Theta1 Theta2 L1 L2
Theta = Theta1 + Theta2;
L = sqrt(L1^2+L2^2-2*L1*L2*cos(pi-Theta2));
Jv_nov = simplify(jacobian([L, Theta], [Theta1 Theta2]));
disp(Jv_nov)

%% 3.22
clc
clear variables

syms Theta1 Theta2 Theta3 L3 L2
A1 = trotz(Theta1)*corrp(trotx(pi/2));
A2 = trotz(Theta2)*transl(1,0,0);
A3 = trotz(Theta3)*transl(1,0,0);
A12 = simplify(A1*A2);
A = simplify(A1*A2*A3);
disp(A)

Jv = jacobian([A(1,4) A(2,4) A(3,4)], [Theta1 Theta2 Theta3]);
disp(Jv);
syms S1 S2 S3 C1 C2 C3
Jv_proba = subs(Jv, [sin(Theta1) sin(Theta2) sin(Theta3) cos(Theta1) cos(Theta2) cos(Theta3)], [S1 S2 S3 C1 C2 C3]);
disp(Jv_proba) % mozhe vaka ama treba mn rabota za dzabe

Jv_k2 = simplify(A12(1:3,1:3) \ Jv);
disp(Jv_k2) % Jv koordinaten sistem 2

Jv_k2det = simplify(det(Jv_k2));
% od tuka se dobiva Theta3 = 0 ili pi
% Theta2 = pi/2
% Theta3 = 0;

%% 3.23
clc
clear variables

Theta1 = 0;
Theta2 = 0;
D1 = 0;
D2 = 0;
D3 = 5;

L(1) = Link([Theta1 D1 0 pi/2]);
L(2) = Link([Theta2 D2 0 pi/2]); % kako tuka da se opredeli DH modelot 
L(3) = Link([0 D3 0 0]);
Robot = SerialLink(L);
Robot.plot([0 pi/2 pi/2]);

syms Theta1 Theta2 D1 D2 D3
A1 = trotz(Theta1)*transl(0,0,0)*corrp(trotx(pi/2));
A2 = trotz(Theta2)*transl(0,0,0)*corrp(trotx(pi/2));
A3 = transl(0,0,D3);
A = simplify(A1*A2*A3);
disp(A) % se dobiva minus pred z
A12 = simplify(A1*A2);
Jv = jacobian([A(1,4) A(2,4) A(3,4)], [Theta1 Theta2 D3]); % znacite se obratno
Jw = [[0; 0; 1] A1(1:3,3) A12(1:3,3)];
J = [Jv; Jw];
disp(J)
J_vrednosti = subs(J, [Theta1 Theta2 D3], [0 0 5]);
disp(J_vrednosti)

% D = J * Dq
Dq = [0.1; -0.1; 0.05];
D = J_vrednosti * Dq;
disp(D)
triangleM = F_triangle(-0.5,0,-0.5,0,0.1,0.2);
disp(triangleM) % se dobivaat razlichni vrednosti 
Avred = subs(A, [Theta1 Theta2 D3], [0 0 5]);
dAvred = triangleM*Avred;
Anova = Avred+dAvred;
disp(Anova)

triangleM_3 = Avred \ triangleM * Avred;
disp(triangleM_3)

%% 3.24
clc
clear variables
syms Theta1 Theta2 Theta3 Theta4 Theta5 Theta6 a2 a3 a4
A1 = trotz(Theta1)*transl(0,0,0)*transl(0,0,0)*corrp(trotx(pi/2));
A2 = trotz(Theta2)*transl(0,0,0)*transl(a2,0,0)*corrp(trotx(0));
A3 = trotz(Theta3)*transl(0,0,0)*transl(a3,0,0)*corrp(trotx(0));
A4 = trotz(Theta4)*transl(0,0,0)*transl(a4,0,0)*corrp(trotx(-pi/2));
A5 = trotz(Theta5)*transl(0,0,0)*transl(0,0,0)*corrp(trotx(pi/2));
A6 = trotz(Theta6)*transl(0,0,0)*transl(0,0,0)*corrp(trotx(0));
A12 = A1*A2;
A13 = A12*A3;
A14 = A13*A4;
A15 = A14*A5;
A16 = simplify(A15*A6);
disp(A16) 
% t6J16 e -nx*py-ny*px = 0
% T6j21 = -ox*py+oy*px = elementi na matricata T06;

%% 3.25
clc
clear variables
syms Theta1 Theta2 Theta3 L1 L2 L3
A1 = trotz(Theta1)*transl(0,0,0)*transl(L1,0,0)*corrp(trotx(pi/2));
A2 = trotz(Theta2)*transl(0,0,0)*transl(L2,0,0)*corrp(trotx(0));
A3 = trotz(Theta3)*transl(0,0,0)*transl(L3,0,0)*corrp(trotx(0));
A = simplify(A1*A2*A3);
disp(A)
Jv = jacobian([A(1,4) A(2,4) A(3,4)], [Theta1 Theta2 Theta3]);
syms C1 C2 C3 S1 S2 S3 S23 C23
disp(subs(Jv, [cos(Theta1) cos(Theta2) cos(Theta3) sin(Theta1) sin(Theta2) sin(Theta3) sin(Theta2 + Theta3) cos(Theta2 + Theta3)], [C1 C2 C3 S1 S2 S3 S23 C23]))
Jvdet = det(Jv);
disp(simplify(Jvdet))
disp(subs(simplify(Jvdet), [cos(Theta1) cos(Theta2) cos(Theta3) sin(Theta1) sin(Theta2) sin(Theta3) sin(Theta2 + Theta3) cos(Theta2 + Theta3)], [C1 C2 C3 S1 S2 S3 S23 C23]))
% Theta3 = 0;
%% DINAMIKA NA ROBOTSKA RAKA
%% Voved
% Lagranzhova mehanika
% Fi = d/dt(dL/dx_tockai)- dL/dxi
% Ti = d/dt(dL/dTheta_tockai)- dL/dThetai
% L = K-P
% dinamichki model za s-m so dva stepeni na sloboda
% T = M(q)*q_dvetocki + C1(q,q_tocka)*q_tocka + C2(q,q_tocka)*q_tocka + G(q)
% M(q) = inerciska matrica
% D11 = efektiven moment na inercija za jazol 1
% D12 = se narekuva zdruzhena inercija
% C1(q,q_tocka) = matrica na centrifugalni sili
% C2(q,q_tocka) = matrica na Koriolisovite sili
% G(q) = gravitaciski vektor
%% Kinetichka energija
% K = 1/2*Suma(i=1 do n){Suma(p=1 do i){Suma(s=1 do i)[trace(Uip*Ji*Uis_transponirana)qp_tocka*qs_tocka]}}
% Qj_trans = [0 0 0 0; 0 0 0 0; 0 0 0 1; 0 0 0 0]
% Qj_rot = [0 -1 0 0; 1 0 0 0; 0 0 0 0; 0 0 0 0]
% Uijk = dUij/dq_k
%% Potencijalna energija
% P = Suma(i=1 do n){-m_i*g^tr*(T0i*r_i)}
%% ZADACHI TEMA 4
%% 4.1
clc 
clear variables 
syms m y(t) g
K = 1/2*m*diff(y, t)^2;
pretty(K);
P = m*g*y;
L = K - P;
pretty(L)
F = rforces(L,y(t));
pretty(F)
%% 4.2
clc
clear variables
syms L Theta(t) m g
x = L*cos(Theta);
y = L*sin(Theta);
K = 1/2*m*(diff(x, t)^2 + diff(y, t)^2);
P = m*g*L*sin(Theta);
L = K - P;
T = rforces(L, Theta);
pretty(simplify(T))
%% 4.3
clc
clear variables
syms m1 m2 d1(t) d2(t) g
K1 = 1/2*m1*diff(d1, t)^2;
K2 = 1/2*m2*(diff(d1, t)^2 + diff(d2, t)^2);
K = K1+K2;
P1 = m1*g*d1;
P2 = m2*g*d1;
P = P1+P2;
L = K-P;
F1 = rforces(L, d1);
F2 = rforces(L, d2);
pretty(simplify(F1));
pretty(simplify(F2));
%% 4.4
clc 
clear variables
syms m1 m2 l1 l2 Theta1(t) Theta2(t) g
x1 = l1*cos(Theta1);
y1 = l2*sin(Theta1);
K1 = 1/2*m1*(diff(x1, t)^2+diff(y1, t)^2);
x2 = x1 + l2*cos(Theta1+Theta2);
y2 = y1 + l2*sin(Theta1+Theta2);
K2 = 1/2*m2*(diff(x2, t)^2+diff(y2, t)^2);
K = K1+K2;
P1 = m1*y1*g;
P2 = m2*y2*g;
P = P1+P2;
L = K-P;
T1 = rforces(L, Theta1);
T2 = rforces(L, Theta2);
disp(simplify(T1));
disp(simplify(T2));
%% 4.5
% ne brisham promenlii i cmw mi trebaat T1 i T2 od 4.4
g1 = subs(T1, [diff(Theta1(t), t, t) diff(Theta2(t), t, t) diff(Theta1(t), t)^2 diff(Theta2(t), t)^2 diff(Theta1(t), t)*diff(Theta2(t), t) diff(Theta1, t) diff(Theta2, t)], [0 0 0 0 0 0 0]);
g2 = subs(T2, [diff(Theta1(t), t, t) diff(Theta2(t), t, t) diff(Theta1(t), t)^2 diff(Theta2(t), t)^2 diff(Theta1(t), t)*diff(Theta2(t), t) diff(Theta1, t) diff(Theta2, t)], [0 0 0 0 0 0 0]);
syms t
g1_vrednosti = subs(g1, [l1 l2 m1 m2 Theta1 Theta2 g], [2 1 5 3 4*sin(t) 4*sin(0.5*t) 9.81]);
g2_vrednosti = subs(g2, [l1 l2 m1 m2 Theta1 Theta2 g], [2 1 5 3 4*sin(t) 4*sin(0.5*t) 9.81]);
%% 4.6
clc
clear variables
syms m r(t) Theta(t) g l
x = r*cos(Theta);
y = r*sin(Theta);
Krot = 1/2*m*(diff(x, t)^2+diff(y, t)^2);
Klin = 1/2*m*diff(r, t)^2;
K = Krot+Klin;
P = m*g*(r*sin(Theta)+l);
L = K-P;
F = rforces(L, r);
T = rforces(L, Theta);
pretty(simplify(F));
pretty(simplify(T));

Theta = 0:0.1:10;
m = 1;
g = 9.81;
r = 0.5;
l = 1;
g1 = g*m*sin(Theta);
g2 = m*r*g*cos(Theta);
plot(Theta,g1)
hold on;
plot(Theta,g2)

%% 4.7
clc 
clear variables
syms d1(t) d2(t) m1 m2 g 

T01 = transl(0,0,d1(t))*corrp(trotx(-pi/2));
T12 = transl(0,0,d2(t));

M = T01;
M(:,:,2) = T01*T12;

joint_variables = [d1(t),d2(t)];

Inertial_matrixes = [0 0 0 0 ; 0 0 0 0; 0 0 0 0; 0 0 0 m1];
Inertial_matrixes(:,:,2) = [0 0 0 0 ; 0 0 0 0; 0 0 0 0; 0 0 0 m2];

gv = [0;0;g;0];

masses = [m1,m2];

r=[0;0;0;1];
r(:,:,2)=[0;0;0;1]; 

L = lagrangianp(M,joint_variables,Inertial_matrixes,gv,masses,r);
pretty(L) 

f1 = rforces(L,d1(t));  
f2 = rforces(L,d2(t));

U11 = Uix(M,1,1,joint_variables);
disp(U11);
U12 = Uix(M,1,2,joint_variables);
disp(U12)

pretty(f1)
pretty(f2)
%% 4.8
clc
clear variables
syms g m1 m2 d1(t) d2(t)

K1 = (1/2)*m1*diff(d1(t))^2;
K2 = (1/2)*m2*(diff(d1(t))^2+diff(d2(t))^2);
K = K1+K2;
P1 = m1*g*d1;
P2 = m2*g*d1;
P = P1+P2;
L=K-P;

f1 = rforces(L,d1(t));
f2 = rforces(L,d2(t));
pretty(f1)
pretty(f2)
% ramnotezhni sostojbi se dobivaat kako d/dt od f = [d1; d1_tocka; d2; d2_tocka]
% d1_tocka = d2_tocka = 0;
% (f1-(m1+m2)*g)/(m1+m2) => f1 = const
% d2/m2 = 0 => d2 = 0
% mozhni se beskonechno mn ramnotezhni sostojbi
%% 4.9
clc
clear variables
syms d1(t) d2(t) d3(t) m1 m2 m3 g
K1 = 1/2*m1*diff(d1)^2;
K2 = 1/2*m2*(diff(d1)^2+diff(d2)^2);
K3 = 1/2*m3*(diff(d1)^2+diff(d2)^2+diff(d3)^2);
K = K1+K2+K3;

P1 = m1*d1*g;
P2 = m2*d1*g;
P3 = m3*d1*g;
P = P1+P2+P3;

L = K-P;

f1 = rforces(L,d1);
f2 = rforces(L,d2);
f3 = rforces(L,d3);

pretty(f1)
pretty(f2)
pretty(f3)

% ako se najdat d1_tocka d2_tocka ... d1 d2 d3 =0 => povtorno 
% deka ima beskonechno mn reshenija
%% 4.10
clc 
clear variables
syms m1 m2 m3 d
M = [m1+m2+m3 0 0; 0 m2+m3 0; 0 0 m3];
Mt = transpose(M);
syms x1 x2 x3
x = [x1; x2; x3];
xMx = transpose(x)*M*x; %xMx treba da e pozitivna >0
disp(xMx) % ili x>0
% => m3I<M<(m1+m2+m3)I
% treba da se dokazhe deka W = 1/2*M_tocka(d)-C(d,d_tocka)
% W+Wt = 1/2*M_tocka(d)-C(d,d_tocka)+transpose(1/2*M_tocka(d)-C(d,d_tocka))
% W+Wt = 1/2*M_tocka+1/2*M_tocka
% W+Wt = M_tocka = 0
M_tocka = diff(M, d);
disp(M_tocka);
% W+Wt = 0 (nulta matrica)
% => W = -Wt
%% 4.11
clc
clear variables
syms Theta1(t) Theta2(t) m1 m2 g I1 I2
l1 = 8;
l2 = 4;

K1 = (1/2)*m1*diff(Theta1(t))^2*(l1/2)^2+(1/2)*I1*diff(Theta2(t))^2;
P1 = m1*g*(l1/2)*sin(Theta1(t));

x2 = l1*cos(Theta1(t))+(l2/2)*cos(Theta1(t)+Theta2(t));
y2 = l1*sin(Theta1(t))+(l2/2)*sin(Theta1(t)+Theta2(t));

v2x = diff(x2);
v2y = diff(y2);

v_kvadrat = (v2x^2+v2y^2);

K2 = (1/2)*m2*v_kvadrat+(1/2)*I2*diff(Theta2(t))^2;
P2 = m2*g*y2;

K = K1+K2;
P = P1+P2;
L = K-P;

T1 = rforces(L,Theta1(t));
T2 = rforces(L,Theta2(t));

disp(T1)
pretty(T2)
%% 4.12
clc 
clear variables
syms r1 r2 m1 m2 Theta1(t) Theta2(t) L1 L2 Ix1 Iy1 Iz1 Ix2 Iy2 Iz2 g
r1 = L1/2;
r2 = L2/2;
x1 = r1*cos(Theta1);
y1 = r1*sin(Theta1);
x2 = L1*cos(Theta1)+r2*cos(Theta1+Theta2);
y2 = L1*sin(Theta1)+r2*sin(Theta1+Theta2);

w1 = [0;0;diff(Theta1, t)];
w2 = [0;0;diff(Theta2, t)];

I1 = [Ix1 0 0; 0 Iy1 0; 0 0 Iz1];
I2 = [Ix2 0 0; 0 Iy2 0; 0 0 Iz2];

K1 = 1/2*m1*(diff(x1)^2+diff(y1)^2)+1/2*m1*transpose(w1)*I1*w1;
K2 = 1/2*m2*(diff(x2)^2+diff(y2)^2)+1/2*m2*transpose(w2)*I2*w2;
K = K1+K2;

P1 = m1*g*y1;
P2 = m2*g*y2;
P = P1+P2;

L = K-P;

T1 = rforces(L, Theta1);
T2 = rforces(L, Theta2);
pretty(simplify(T1));
disp(simplify(T2));
%% 4.13
clc
clear variables
syms m1 m2 d1(t) Theta2(t) g l2

x2 = d1 + l2*cos(Theta2);
y2 = -l2*sin(Theta2);
K1 = 1/2*m1*diff(d1)^2;
K2 = 1/2*m2*(diff(x2)^2+diff(y2)^2);
K = K1+K2; 

P1 = m1*0;
P2 = -m2*y2*g;
P = P1+P2;

L = K-P;

F1 = simplify(rforces(L, d1));
T2 = simplify(rforces(L, Theta2));

pretty(vpa(F1))
pretty(T2) %greshka vo zadachata ne se sovpagjaat reshenijata vo zbirkata
%% 4.14
clc
clear variables
syms m1 m2 m3 d1 L2 L3 Theta2 Theta3

x = d1+L2*cos(Theta2)+L3*cos(Theta2+Theta3);
y = -L2*sin(Theta2)-L3*sin(Theta2+Theta3);
alpha = Theta2+Theta3;

Jv = jacobian([x y alpha], [d1 Theta2 Theta3]);
% greshka vo jakobijanot
% singulariteti so detJv
disp(Jv)
syms d1(t) Theta2(t) Theta3(t) m1 m2 m3 L2 L3
x1 = d1;
y1 = 0;

x2 = d1+L2*cos(Theta2);
y2 = -L2*sin(Theta2);

x3 = x2+L3*cos(Theta2+Theta3);
y3 = y2 - L3*sin(Theta2+Theta3);

K1 = 1/2*m1*(diff(x1)^2);
K2 = 1/2*m2*(diff(x2)^2+diff(y2)^2);
K3 = 1/2*m3*(diff(x3)^2+diff(y3)^2);
K = K1+K2+K3;
disp(K)

%% 4.15
clc
clear variables
syms m1 m2 g L1 L2 Alpha Theta(t)
J = m2*L2*cos(Alpha)^2;
K = 1/2*J*diff(Theta)^2;
P = m1*L1*g + m2*(L2*sin(Alpha)+L1)*g;
L = K-P;
disp(L)
T = rforces(L, Theta);
disp(T)
% matematichki model
MM = diff([Theta; diff(Theta)]);
% se dobiva deka Theta_tocka = 0
% i deka T/(m2*L2*cos(Alpha)^2) = 0 => T = 0;
% beskonechno mnogu ramnotezhni sostojbi
%% 4.16
clc
clear variables
syms Theta(t) L(t) R(t) g m1 m2 J
K1 = 1/2*J*diff(Theta)^2 + 1/2*m1*diff(L)^2;
K2 = 1/2*m2*R^2*diff(Theta)^2 + 1/2*m2*(diff(R)^2+diff(L)^2);
K = K1+K2;
P = m1*g*L + m2*g*L;
Lag = K-P;
T1 = rforces(Lag, Theta);
F2 = rforces(Lag, L);
F3 = rforces(Lag, R);
pretty(T1)
pretty(F2)
pretty(F3)
%% 4.17
clc
clear variables
syms q1 q2 q3 m2 I2 m3 I3 m4 L3
T01 = transl(0,0,q1);
T12 = trotz(q2)*corrp(trotx(-pi/2));
T02 = T01*T12;
T23 = trotz(q3);
disp(T23)
T03 = simplify(T01*T12*T23);
disp(T03)
T04 = T03*transl(L3,0,0);
disp(T04)

Jv = simplify(jacobian([T04(1,4) T04(2,4) T04(3,4)], [q1 q2 q3]));
disp(Jv)

Jv_3 = simplify([T03(1,1:3); T03(2,1:3); T03(3,1:3)]\Jv);
disp(Jv_3)

syms dx2 dy2 dz2 dq1 dq2 dq3 dx3 dy3 dz3
Jv_01 = zeros(3);
Jv_01(3,1)=1;
Jv_02 = [dx2; dy2; dz2]/[dq1; dq2; dq3];
Jv_03 = [dx3; dy3; dz3]/[dq1; dq2; dq3];
Jv_02vred = subs(Jv_02, [dx2/dq1 dy2/dq1 dz2/dq1], [0 0 1]);
Jv_03vred = subs(Jv_03, [dx3/dq1 dy3/dq1 dz3/dq1], [0 0 1]);
disp(Jv_02vred);
disp(Jv_03vred);

% M = Suma(i=1 do n){mi*transpose(Jvi)*Jvi}+Suma(i=1 do n){transpose(Jwi)*Ii*Jwi}
Jv_matricamatrici = Jv_01;
Jv_matricamatrici(:,:,2) = Jv_02vred;
Jv_matricamatrici(:,:,3) = Jv_03vred; 

Jw_matricamatrici = zeros(3);
Jw_matricamatrici(:,:,2) = [0 0 0; 0 0 0; 1 0 0];
Jw_matricamatrici(:,:,3) = [0 0 0; 0 0 0; 1 0 0];

syms m1 m2 m3 g Ix Iy Iz
m = [m1 m2 m3];
I = [Ix 0 0; 0 Iy 0; 0 0 Iz];

M = masses_mat(m, Jv_matricamatrici, Jw_matricamatrici, 3, I);

%% 4.18
clc
clear variables
syms Theta1 Theta2 Theta3 Theta4 C12 S12 C34 S34 S1 S2 S3 S4 C1 C2 C3 C4
T04 = [C12*C34-1/sqrt(2)*S12*S34 -C12*S34-1/sqrt(2)*S12*C34 1/sqrt(2)*S12 sqrt(2)*C12*C3-S12*(S3-1)+C1
       S12*C34+1/sqrt(2)*C12*S34 -S12*S34-1/sqrt(2)*C12*C34 1/sqrt(2)*C12 sqrt(2)*S12*C3+C12*(S3-1)+S1
       1/sqrt(2)*S34 1/sqrt(2)*C34 1/sqrt(2) S3+1
       0 0 0 1];
T04 = subs(T04, [C12 S12 C34 S34 S1 S2 S3 S4 C1 C2 C3 C4], [cos(Theta1+Theta2) sin(Theta1+Theta2) cos(Theta3+Theta4) sin(Theta3+Theta4) sin(Theta1) sin(Theta2) sin(Theta3) sin(Theta4) cos(Theta1) cos(Theta2) cos(Theta3) cos(Theta4)]);
disp(T04)

Jw_0 = [0 0 1/sqrt(2) 1/sqrt(2); 0 0 0 0; 1 1 1/sqrt(2) 1/sqrt(2)];

Jv = simplify(jacobian([T04(1,4) T04(2,4) T04(3,4)], [Theta1 Theta2 Theta3 Theta4]));
disp(Jv)

Jv_0 = subs(Jv, [Theta1 Theta2 Theta3 Theta4], [0 pi/2 -pi/2 0]);
disp(Jv_0)

J = [Jv_0; Jw_0];
pretty(J)

F_44 = [0; 6; 0; 7; 0; 8];
% T = transpose(J)*F = transpose(J)*F_04

T = transpose(J)*F_44; % ne se vo ist koordinaten sistem 
disp(T);

%% 4.19
clc
clear variables
syms l1 l2 l3 Theta1 Theta2 Theta3
x = l1*cos(Theta1)+l2*cos(Theta1+Theta2)+l3*cos(Theta1+Theta2+Theta3);
y = l1*sin(Theta1)+l2*sin(Theta1+Theta2)+l3*sin(Theta1+Theta2+Theta3);
Jv = jacobian([x,y],[Theta1,Theta2,Theta3]);

Jv = subs(Jv,l1,3);
Jv = subs(Jv,l2,2);
Jv = subs(Jv,l3,1);
Jv = subs(Jv,Theta1,degtorad(135));
Jv = subs(Jv,Theta2,degtorad(45));
Jv = subs(Jv,Theta3,degtorad(225));
T = transpose(Jv)*[10;-2];
vpa(T)

%% 4.20
clc
clear variables
syms ThetaL(t) R Jm Jl M L g
ThetaM = ThetaL*R;
K = 1/2*Jm*diff(ThetaM)^2+1/2*Jl*diff(ThetaL)^2;
P = M*g*L*(1-cos(ThetaL));
Lag = K-P;
T1 = rforces(Lag, ThetaL);
pretty(T1)
%% 4.21
clc
clear variables
syms N Im ThetaM Theta(t) L m I g
K = 1/2*(m*L^2+I)*diff(Theta)^2+1/2*Im*N^2*diff(Theta)^2;
P = m*g*L*(L-cos(Theta));
L = K-P;
T1 = rforces(L, Theta(t));
pretty(T1);
%% 4.22
clc
clear variables
% M(q)*q_dvetocki+C(q,q_tocka)*q_tocka+g(q) = T
% D_tocka = J*q_tocka
% D_tocka = [transpose(V) transpose(W)]
% q_tocka = trasnpose([Theta1_tocka Theta2_tocka])
% T = transpose(J)*F
% D_dvetocki=J*q_dvetocki+J_tocka*q_tocka
% q_dvetocki = J^-1*D_dvetocki - J_tocki*q_tocka
% M(q){J^-1*D_dvetocki-J^-1*J_tocka*q_tocka}+C(q,q_tocka)+g(q) = 
% M(q){J^-1*D_dvetocki-J^-1*J_tocka*J^-1*D_tocka}+C(q,q_tocka)*J^-1*D_tocka+g(q) = transpose(J)*F 
% str(250)
%% 4.23
clc
clear variables
J_T6 = [20 0 0 0 0 0; -5 0 1 0 0 0; 0 20 0 0 0 0; 0 1 0 0 1 0; 0 0 0 1 0 0; -1 0 0 0 0 1];
F_T6 = [0; 0; 5; 0; 0; 20];
T = transpose(J_T6)*F_T6;
disp(T);
%% 4.24
clc
clear variables
syms Theta1 Theta2 L1 L2 L0
A1 = trotz(Theta1)*transl(L1,0,0);
A2 = trotz(Theta2)*transl(L2,0,0);
A = A1*A2;
Jv = simplify(jacobian([A(1,4) A(2,4) A(3,4)], [Theta1 Theta2]));
Jv = subs(Jv, [Theta1 Theta2 L1 L2], [pi/2 -pi/4 0.3 0.5]);
vpa(Jv, '2')
F_nadvor = [0; -500];
F_vnatre = [0; 500];
T = transpose(vpa(Jv(1:2,1:2),'2'))*F_vnatre;
vpa(T, '2') % greshka ne se sovpagjaat so reshenijata vo zbirkata 175 175 se vo zbirka
%% 4.25
clc
clear variables
F_U = transpose([0 45 0 0 0 35]);
H = [0 1 0 2; 0 0 1 4; 1 0 0 7; 0 0 0 1];
f = F_U(1:3,1);
m = F_U(4:6,1);
disp(m)
P = H(1:3,4);
disp(P)
n = H(1:3,1);
o = H(1:3,2);
a = H(1:3,3);
F_Hx = transpose(n)*f;
F_Hy = transpose(o)*f;
F_Hz = transpose(a)*f;
syms i j k
fxP = det([1i 1i k; 0 45 0; 2 4 7]);
m_v = [1i 1i k]*[0; 0; 35];
fxPm = m_v + fxP;
disp(fxPm)
fxPmv = [315; 0; -55];
M_Hx = transpose(n)*fxPmv;
M_Hy = transpose(o)*fxPmv;
M_Hz = transpose(a)*fxPmv;
F_H = [F_Hx F_Hy F_Hz M_Hx M_Hy M_Hz];
disp(F_H);
%% 4.26
clc 
clear variables
F_U = transpose([10 0 5 15 20 0]);
H = [0.7071 0.7071 0 2
     0 0 1 1
     0.7071 -0.7071 0 5
     0 0 0 1];
f = F_U(1:3,1);
% m = F_U(4:6,1);
% P = H(1:3,4);
n = H(1:3,1);
o = H(1:3,2);
a = H(1:3,3);
F_Hx = transpose(n)*f;
F_Hy = transpose(o)*f;
F_Hz = transpose(a)*f;
syms i j k
fxP = det([1i 1i k; 0 45 0; 2 1 5]);
m_v = [1i 1i k]*[0; 0; 35];
fxPm = m_v + fxP;
disp(fxPm)
fxPmv = [315; 0; -55];
M_Hx = transpose(n)*fxPmv;
M_Hy = transpose(o)*fxPmv;
M_Hz = transpose(a)*fxPmv;
F_H = [F_Hx F_Hy F_Hz M_Hx M_Hy M_Hz];
disp(F_H); % greshni se vrednostite za Mx..
%% 4.27
clc
clear variables
F_U = transpose([20 10 0 100 0 0]);
H = [0 -0.7071 0.707000 4
     1 0 0 10
     0 0.7071 1.707 5
     0 0 0 1];
f = F_U(1:3,1);
m = F_U(4:6,1);
P = H(1:3,4);
n = H(1:3,1);
o = H(1:3,2);
a = H(1:3,3);
F_Hx = transpose(n)*f;
F_Hy = transpose(o)*f;
F_Hz = transpose(a)*f;
syms li lj lk
fxP = det([li li lk; 0 45 0; 2 1 5]); %ista matrica kako prethodnoto reshenie a razlichni vrednosti
m_v = [li li lk]*[0; 0; 35];
fxPm = m_v + fxP;
fxPmv = [315; 0; -55];
M_Hx = transpose(n)*fxPmv;
M_Hy = transpose(o)*fxPmv;
M_Hz = transpose(a)*fxPmv;
F_H = [F_Hx F_Hy F_Hz M_Hx M_Hy M_Hz];
disp(F_H);
%% 4.28
clc
clear variables
syms L1 L2 D3 Theta1 Theta2
P_0E = [L1*cos(Theta1)+L2*sin(Theta1)-D3*cos(Theta1)*sin(Theta2)
        L1*sin(Theta1)+L2*cos(Theta1)-D3*sin(Theta1)*sin(Theta2)
        D3*cos(Theta2)];
Jv_0 = jacobian(transpose(P_0E), [Theta1 Theta2 D3]);
disp(Jv_0)
Jv_0vred = subs(Jv_0, [Theta1 Theta2 D3], [pi/2 pi/6 2*L1]);
pretty(Jv_0vred)
% T = transpose(J_0)*F_03 = transpose(J_0)*R_03*F_33
A1 = trotz(Theta1)*transl(L1,0,0)*corrp(trotx(-pi/2));
A2 = trotz(Theta2)*transl(0,0,L2)*corrp(trotx(-pi/2));
A3 = transl(0,0,D3);
A = A1*A2*A3;
disp(A)
F_33 = [1; 0; 0];
T = transpose(Jv_0vred)*A(1:3,1:3)*F_33;
disp(T) % se dobiva razlichno reshenie kaj DH modelot
%% 4.29
clc
clear variables
L = [1; 0.5; 1];
agli = [pi/6; 2; pi/4];
syms L1 L2 L3 Theta1 D2 Theta3
x = L1*cos(Theta1)+D2*cos(Theta1)-L2*sin(Theta1)+L3*cos(Theta1+Theta3);
y = L1*sin(Theta1)+D2*sin(Theta1)+L2*cos(Theta1)+L3*sin(Theta1+Theta3);
fi = Theta1+Theta3;
J0 = jacobian([x y fi], [Theta1 D2 Theta3]);
F0 = transpose([20 10 5]);
T = transpose(subs(J0, [L1 L2 L3 Theta1 D2 Theta3], [1 0.5 1 pi/6 2 pi/4]))*F0;
T = vpa(T, '4');
disp(T)
%% PLANIRANJE TRAEKTORII NA ROBOTSKA RAKA
%% Voved
% Theta(t0) = c0 = Theta0 
% Theta(tf) = c3*tf^3+c2*tf^2+c1*tf+c0 = Thetaf
% Theta_tocka(t0) = 3*c3*t0^2+2*c2*t0+c1 = c1 = 0
% Theta_tocka(tf) = 3*c3*tf^2+2*c2*tf+c1 = 0
%% ZADACHI TEMA 5
%% 5.1
clc 
clear variables
% t = 5s, Theta_t0 = 30deg, Theta_tf = 75deg
% da se presmetaat vrednostite na agolot na zglobot
% Theta_t0 = c0;
c0 = 30;
% Theta_tf = 5^3*c3+5^2*c2+5*c1+c0 = 75;
% Theta_tocka_t0 = c1
c1 = 0;
% Theta_tocka_tf = 3*5^2*c3+2*5*c2+c1 = 0;
syms c3 c2
r1 = 5^3*c3+5^2*c2+5*c1+c0-75;
r2 =  3*5^2*c3+2*5*c2+c1;
disp(r1)
disp(r2)
v1 = [25 125];
v2 = [10 75];
v = [v1; v2];
r = [45; 0];
Reshenija = v \ r;
c2 = Reshenija(1,1);
c3 = Reshenija(2,1);
disp(Reshenija)
syms Theta(t) t
Theta(t) = vpa(c3*t^3+c2*t^2+c1+c0); % polozhba
pretty(Theta)
Theta_tocka(t) = diff(Theta, t); 
pretty(Theta_tocka) % brzina
Theta_dvetocki(t) = diff(Theta_tocka, t);
pretty(Theta_dvetocki) % zabrzuvanje
Theta1 = subs(Theta, t, 1);
disp(Theta1);
Theta2 = subs(Theta, t, 2);
disp(Theta2);
Theta3 = subs(Theta, t, 3);
disp(Theta3);
Theta4 = subs(Theta, t, 4);
disp(Theta4);
t = 0:0.1:5;
plot(t, Theta(t))
hold on;
plot(t, Theta_tocka(t))
hold on;
plot(t, Theta_dvetocki(t))
%% 5.2
clc
clear variables
% prodolzhenie na zadacha 5.1
% Theta_tf=105deg
% t = 3sec, bidejki e prodolzhenie na prethodnata zadacha Theta_t0 = 75deg
c0 = 75;
c1 = 0;
% Theta_tf = 3^3*c3+3^2*c2+3*c1+c0 = 105
% Theta_tockatf = 3*3^2*c3+2*3*c2+c1 = 0
syms c2 c3
r1 = 3^3*c3+3^2*c2+3*c1+c0-105;
r2 = 3*3^2*c3+2*3*c2+c1;
disp(r1)
disp(r2)
v = [9 27; 6 27];
p = [-30; 0];
res = v \ p;
disp(res)
c3 = -2.2222;
c2 = 10;
syms Theta(t) t
Theta(t) = vpa(c3*t^3+c2*t^2+c1+c0); % polozhba
pretty(Theta)
Theta_tocka(t) = diff(Theta, t); 
pretty(Theta_tocka) % brzina
Theta_dvetocki(t) = diff(Theta_tocka, t);
pretty(Theta_dvetocki) % zabrzuvanje
t = 5:0.1:8;
plot(t, Theta(t))
hold on;
plot(t, Theta_tocka(t))
hold on;
plot(t, Theta_dvetocki(t)) % treba da se spojat grafikot od ovaa so grafikot od rpethodnata zadacha
%% 5.3
clc 
clear variables
% Theta(t) = a3*t^3+a2^t^2+a1*t+a0
% Theta_i = 50deg pochetna
% Theta_f = 80deg, t = 3sec krajna
a0 = 50;
a1 = 0;
syms a2 a3 t
r1 = subs(a3*t^3+a2*t^2+a1*t+a0-80, t, 3);
r2 = subs(diff(a3*t^3+a2*t^2+a1*t+a0, t), t, 3);
disp(r1)
disp(r2)
v = [9 27; 6 27];
p = [30; 0];
resh = v \ p;
disp(resh)
a2 = 10;
a3 = -20/9;
syms Theta(t) t
Theta(t) = a3*t^3+a2*t^2+a1*t+a0;
pretty(Theta)
Theta_tocka(t) = diff(Theta, t); 
pretty(Theta_tocka) % brzina
Theta_dvetocki(t) = diff(Theta_tocka, t);
pretty(Theta_dvetocki) % zabrzuvanje
t = 0:0.1:3;
plot(t, Theta(t))
hold on;
plot(t, Theta_tocka(t))
hold on;
plot(t, Theta_dvetocki(t)) 
vpa(Theta(1))
vpa(Theta(2))
vpa(Theta(3))
%% 5.4
clc 
clear variables
% Theta(t) = a3*t^3+a2*t^2+a1*t+a0
% Theta_i = 20deg pochetna
% Theta_f = 80deg, t = 4sec krajna
a0 = 20;
a1 = 0;
syms a2 a3 t
r1 = subs(a3*t^3+a2*t^2+a1*t+a0-80, t, 4);
disp(r1)
r2 = subs(diff(a3*t^3+a2*t^2+a1*t+a0-5, t), t, 4);
disp(r2)
v = [16 64; 8 48];
p = [60; 5];
resh = v \ p;
disp(resh)
a2 = 10;
a3 = -25/16;
syms Theta(t) t
Theta(t) = a3*t^3+a2*t^2+a1*t+a0;
pretty(Theta)
Theta_tocka(t) = diff(Theta, t); 
pretty(Theta_tocka) % brzina
Theta_dvetocki(t) = diff(Theta_tocka, t);
pretty(Theta_dvetocki) % zabrzuvanje
t = 0:0.1:3;
plot(t, Theta(t))
hold on;
plot(t, Theta_tocka(t))
hold on;
plot(t, Theta_dvetocki(t)) 
%% 5.5
clc
clear variables
% do megjupolozhba
syms a0 a1 a2 a3 t
a0 = 20;
a1 = 0;
r1 = subs(a3*t^3+a2*t^2+a1*t+a0-80, t, 5);
r2 = subs(diff(a3*t^3+a2*t^2+a1*t+a0, t), t, 5);
disp(r1)
disp(r2)
v = [25 125; 10 75];
p = [60; 0];
resh = v \ p;
disp(resh)
a2 = 7.2;
a3 = -0.96;
syms Theta(t) t
Theta(t) = a3*t^3+a2*t^2+a1*t+a0;
pretty(Theta)
Theta_tocka(t) = diff(Theta, t); 
pretty(Theta_tocka) % brzina
Theta_dvetocki(t) = diff(Theta_tocka, t);
pretty(Theta_dvetocki) % zabrzuvanje 
t = 0:0.1:5;
plot(t, Theta(t))
hold on;
plot(t, Theta_tocka(t))
hold on;
plot(t, Theta_dvetocki(t))
%vtor del
syms a00 a11 a22 a33 t
a00 = 80;
a11 = 0;
r1 = subs(a33*t^3+a22*t^2+a11*t+a00-25, t, 5);
r2 = subs(diff(a33*t^3+a22*t^2+a11*t+a00, t), t, 5);
disp(r1)
disp(r2)
v = [25 125; 10 75];
p = [55; 0];
resh = v \ p;
disp(resh)
a22 = 33/5;
a33 = -22/25;
syms Theta(t) t
Theta(t) = a33*t^3+a22*t^2+a11*t+a00;
pretty(Theta)
Theta_tocka(t) = diff(Theta, t); 
pretty(Theta_tocka) % brzina
Theta_dvetocki(t) = diff(Theta_tocka, t);
pretty(Theta_dvetocki) % zabrzuvanje 
t = 0:0.1:5;
plot(t, Theta(t))
hold on;
plot(t, Theta_tocka(t))
hold on;
plot(t, Theta_dvetocki(t)) % treba piecewise
%% 5.6
clc
clear variables
% Theta_dvetockiI = 5stepeni/sec^2 
% Theta_dvetockiF = -5stepeni/sec^2
% ThetaI = 30
% thetaF = 75
% t = 5sec
% polinom od 5ti red
c0 = 30;
c1 = 0;
c2 = 5/2;
t = 5;
syms c3 c4 c5 
Thetaf = c5*t^5+c4*t^4+c3*t^3+c2*t^2+c1*t+c0-75;
Theta_tockaf = 5*c5*t^4+4*c4*t^3+3*c3*t^2+2*c2*t+c1;
Theta_dvetockif = 20*c5*t^3+12*c4*t^2+6*c3*t+2*c2+5;
disp(Thetaf)
disp(Theta_tockaf)
disp(Theta_dvetockif)
v = [3125 625 125; 3125 500 75; 2500 300 30];
p = [-35/2 -25 -10];
resh = v \ transpose(p);
disp(resh)
c5 = 0.0464;
c4 = -0.58;
c3 = 1.6;
syms t
Thetaf = c5*t^5+c4*t^4+c3*t^3+c2*t^2+c1*t+c0;
Theta_tockaf = diff(Thetaf, t);
Theta_dvetockif = diff(Theta_tockaf, t);
pretty(Thetaf)
pretty(Theta_tockaf)
pretty(Theta_dvetockif)
%% 5.7
clc 
clear variables
% Theta_dvetockiI = 10stepeni/sec^2 
% Theta_dvetockiF = -10stepeni/sec^2
% ThetaI = 0
% thetaF = 75
% t = 3sec
% polinom od 5ti red
c0 = 0;
c1 = 0;
c2 = 5;
t = 3;
syms c3 c4 c5 
Thetaf = c5*t^5+c4*t^4+c3*t^3+c2*t^2+c1*t+c0-75;
Theta_tockaf = 5*c5*t^4+4*c4*t^3+3*c3*t^2+2*c2*t+c1;
Theta_dvetockif = 20*c5*t^3+12*c4*t^2+6*c3*t+2*c2+5;
disp(Thetaf)
disp(Theta_tockaf)
disp(Theta_dvetockif)
v = [243 81 27; 405 108 27; 540 108 18];
p = [30 -30 -15];
resh = v \ transpose(p);
disp(resh)
c5 = 1.5741;
c4 = -11.6667;
c3 = 21.9444;
syms t
Thetaf = vpa(c5*t^5+c4*t^4+c3*t^3+c2*t^2+c1*t+c0);
Theta_tockaf = vpa(diff(Thetaf, t));
Theta_dvetockif = vpa(diff(Theta_tockaf, t));
pretty(Thetaf)
pretty(Theta_tockaf)
pretty(Theta_dvetockif)
%% 5.8
clc
clear variables
% Thetai = 30
% Thetaf = 120
% tf = 4
% tb = ?
% w = 30rad/s
% tb = (Thetai-Thetaf+w*tf)/w
tb = (30-120+30*4)/30;
disp(tb)
% Pocheten parabolichen del
% Theta(t) = Thetai+w/(2*tb)*t^2 = 30+15*t^2
% Theta_tocka(t) = w/tb*t = 30*t
% Theta_dvetocki(t) = w/tb = 30

% Linearen segmet
% Theta(t) = ThetaA - w*tb +wt
% Theta_tockaA = Theta_tockaB = w 
% Theta_dvetockiA = Theta_dvetockiB = 0

% Kraen parabolichen segment
% Theta(t) = Thetaf - w/(2*tb)*(tf-t)^2 = 120-15(4-t)^2
% Theta_tocka(t) = w/tb*(tf-t) = 30*(4-t)
% Theta_dvetocki(t) = -w/tb = -30
%% 5.9
clc
clear variables
% vidi zbirka
%% 5.10
clc
clear variables
% vidi zbirka
%% 5.11
clc
clear variables
% vidi zbirka
%% 5.12
clc
clear variables
Theta1_nula = 45;
Theta2_nula = -150;
Theta1_f = 90;
Theta2_f = -45;
tf = 2;
tb = 0.5;
w1 = (Theta1_nula - Theta1_f)/(tb - tf);
Theta_dvetocki1 = w1/tb;
w2 = (Theta2_nula - Theta2_f)/(tb - tf);
Theta_dvetocki2 = w2/tb;

% pocheten segment
syms t Theta1(t)
b1 = 0;
Theta_tocka = 60*t;
c1 = 60;
Theta_dvetocki = c1;
a1 = 45;
Theta1(t) = 1/2*c1*t^2 + b1*t + a1;
disp(Theta1(0.5))
c2 = 140;
b2 = 0;
a2 = -150;
syms Theta2(t) t
Theta2(t) = 1/2*c2*t^2 + b2*t + a2;
disp(Theta2(0.5))

% linearen segment
syms Theta1(t) Theta2(t) t 
Theta1(t) = w1*(t-tb)+Theta1_nula+(w1*tb)/2;
Theta2(t) = w2*(t-tb)+Theta2_nula+(w2*tb)/2;
pretty(Theta1(t))
pretty(Theta2(t))

% parabolichen segment
syms Theta1(t) Theta2(t) t 
Theta1(t) = Theta1_f - w1/(2*tb)*(tf-t)^2;
Theta1_tocka = diff(Theta1(t), t);
disp(Theta1_tocka)
Theta1_dvetocki = diff(Theta1_tocka, t);
disp(Theta1_dvetocki)

Theta2(t) = Theta2_f - w2/(2*tb)*(tf-t)^2;
Theta2_tocka = diff(Theta2(t), t);
Theta2_dvetocki = diff(Theta2_tocka, t);
disp(Theta2_dvetocki)
%% 5.13
clc
clear variables
% vidi zbirka
%% 5.14
% vidi zbirka