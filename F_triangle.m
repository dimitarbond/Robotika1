% функција за одредување деференцијален оператор
% прима dx dy dz deltax deltay deltaz враќа triangle
function R = F_triangle(dx,dy,dz,deltax,deltay,deltaz)
R = [0 -deltaz deltay dx; 
           deltaz 0 -deltax dy; 
           -deltay deltax 0 dz;
           0 0 0 0];
end
